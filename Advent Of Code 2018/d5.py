from operator import itemgetter
log = ''

with open("d5.txt") as f:
	for line in f:
		log = line


def polymerize(string):
	i = 0
	while i < len(string) - 1:
		if string[i].lower() == string[i+1].lower() and ((string[i].islower() and string[i+1].isupper()) or (string[i+1].islower() and string[i].isupper())):
			string = string[:i]+string[i+2:]
			if i > 0:
				i -= 1
		else:
			i += 1
	return string


shrinked = polymerize(log)
print(len(shrinked))
shortest = sorted([(x, len(polymerize(log.replace(x, "").replace(x.upper(), "")))) for x in sorted(set(log.lower()))],
				  key=itemgetter(1))[0]
print(shortest)
