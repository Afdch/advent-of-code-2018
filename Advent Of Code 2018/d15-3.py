from operator import attrgetter
import numpy as np
from PIL import Image

mapz = []
images = []
with open('d15.txt') as f:
    for line in f:
        mapz.append(list(map(int, (line.strip().replace(".", '0 ').
                                   replace('#', '1 ').replace('G', '2 ').
                                   replace('E', '3 ')).strip().split(' '))))
mapz = np.array(mapz)


def neighbour_cells(y, x):
    """returns a list of tuples of coordinates for a given coordinate,
    if neighbouring cell is not a wall"""
    nbrs = []
    if mapz[y - 1, x] != 1:
        nbrs.append((y - 1, x))
    if mapz[y, x - 1] != 1:
        nbrs.append((y, x - 1))
    if mapz[y, x + 1] != 1:
        nbrs.append((y, x + 1))
    if mapz[y + 1, x] != 1:
        nbrs.append((y + 1, x))
    return nbrs


class Unit:
    def __init__(self, _y, _x, _type):
        self.x = _x
        self.y = _y
        self.type = _type
        self.hp = 200
    
    def move(self, y, x):
        mapz[self.y, self.x] = 0
        self.y, self.x = y, x
        mapz[self.y, self.x] = self.type
    
    def neighbour_enemies(self, y = -1, x = -1):
        if (y, x) == (-1, -1):
            y = self.y
            x = self.x
        enemies = []
        if mapz[y - 1, x] == (3 if self.type == 2 else 2):
            enemies.append((y - 1, x))
        if mapz[y, x - 1] == (3 if self.type == 2 else 2):
            enemies.append((y, x - 1))
        if mapz[y, x + 1] == (3 if self.type == 2 else 2):
            enemies.append((y, x + 1))
        if mapz[y + 1, x] == (3 if self.type == 2 else 2):
            enemies.append((y + 1, x))
        return enemies
    
    def find_enemy(self):
        """lookup from the unit's position for a unit of other type
        or until no valid moves can be done
        return todo what??
        or None if no path"""
        
        frontier = [(1, (y, x)) for y, x in neighbour_cells(self.y, self.x) if mapz[y, x] == 0]
        f_distance = 0
        came_from = dict({(y, x): None for d, (y, x) in frontier})
        distances = dict({(y, x): d for d, (y, x) in frontier})
        valid_tiles = []
        
        while len(frontier) > 0 and (len([(d, c) for d, c in frontier if d <= f_distance]) > 0 if f_distance > 0 else True):
            frontier.sort(key = lambda z: z[1][1])
            frontier.sort(key = lambda z: z[1][0])
            frontier.sort(key = lambda z: z[0])
            distance, coord = frontier.pop(0)
            
            # check if neighbouring tiles from current have enemies;
            # if they do, do not increase radius further and
            # add all tiles with enemies for this distance to the valid tile list
            ne = self.neighbour_enemies(coord[0], coord[1])
            if len(ne) > 0:
                if f_distance == 0:
                    f_distance = distance
                if distance == f_distance:
                    valid_tiles.append(coord)
            
            for tile in neighbour_cells(coord[0], coord[1]):
                if tile not in distances.keys() or distance + 1 < distances[tile]:
                    if len([1 for unit in units if (unit.y, unit.x) == tile if unit not in killed_units]) == 0:
                        frontier.append((distance + 1, tile))
                        distances[tile] = distance + 1
                        came_from[tile] = coord
        if len(valid_tiles) == 0:
            return None
        else:
            # not sure if I need to sort this but would
            valid_tiles.sort(key = lambda z: z[1])
            valid_tiles.sort(key = lambda z: z[0])
            selected = valid_tiles[0]
            while came_from[selected] is not None:
                selected = came_from[selected]
            return selected


def mk_img(mode = 'gif'):
    size = mapz.transpose().shape
    image = Image.new('RGB', size, "white")
    pixels = image.load()
    for y in range(size[1]):
        for x in range(size[0]):
            if mapz[y, x] == 1:
                pixels[x, y] = 0, 0, 0
            elif mapz[y, x] == 2:
                pixels[x, y] = 0, 255, 0
            elif mapz[y, x] == 3:
                pixels[x, y] = 255, 0, 0
    
    # image.show()
    scale_factor = 8
    image = Image.Image.resize(image, (size[0]*scale_factor, size[1]*scale_factor))
    if mode == 'gif':
        images.append(image)
    elif mode == 'show':
        image.show()
    elif mode == 'save':
        image.save('d15.png')


u = np.where(mapz > 1)
units = [Unit(u[0][i], u[1][i], mapz[u[0][i], u[1][i]]) for i in range(len(u[0]))]
elves = [unit for unit in units if unit.type == 3]
goblins = [unit for unit in units if unit.type == 2]
round_i = 0
while len(elves) > 0 and len(goblins) > 0:
    ended_early = False
    units.sort(key = attrgetter("x"))
    units.sort(key = attrgetter("y"))
    killed_units = []
    for unit in units:
        if unit not in killed_units:
            # Check for eglible targets:
            if unit.type == 2:
                enemies_list = [elf for elf in elves if elf not in killed_units]
            else:
                enemies_list = [goblin for goblin in goblins if goblin not in killed_units]
            if len(enemies_list) > 0:
                # Check for enemies close
                target = unit.neighbour_enemies()
                # region Movement
                if len(target) == 0:
                    move_to = unit.find_enemy()
                    if move_to is not None:
                        unit.move(move_to[0], move_to[1])
                        target = unit.neighbour_enemies()
                # endregion
                
                # region Combat
                if len(target) > 0:
                    enemies = [enemy for enemy in units if unit.type != enemy.type
                               and (enemy.y, enemy.x) in target
                               and unit not in killed_units]
                    enemies.sort(key = attrgetter('hp'))
                    # part 1
                    # enemies[0].hp -= 3
                    
                    # part 2 - solved by just testing various damage:
                    # I am too lazy to automate this too.
                    enemies[0].hp -= 3 if unit.type == 2 else 23
                    
                    if enemies[0].hp <= 0:
                        killed_units.append(enemies[0])
                        mapz[enemies[0].y, enemies[0].x] = 0
                        # part 2
                        if enemies[0].type == 3:
                            print("Elves are too weak!")
                # endregion
            else:
                ended_early = True
    units = [unit for unit in units if unit not in killed_units]
    elves = [unit for unit in units if unit.type == 3]
    goblins = [unit for unit in units if unit.type == 2]
    if not ended_early:
        round_i += 1
    
    mk_img()

images[0].save('d15.gif', save_all = True, append_images = images[1:], duration = 1, loop = 0)

with Image.open('d15.gif') as f:
    f.info['duration'] = 400
    f.save('d15-2.gif', save_all = True, duration = 400)
print(round_i, sum([unit.hp for unit in units]), round_i*sum([unit.hp for unit in units]))
for unit in units:
    print(unit.type, unit.hp, unit.y, unit.x)
mk_img('show')
