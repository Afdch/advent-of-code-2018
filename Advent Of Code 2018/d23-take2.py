import numpy as np
import re
import os
import pyrr 
import networkx as nx
import matplotlib.pyplot as plt

bots = nx.DiGraph()
rx = re.compile(r'pos=\<(-?\d+,-?\d+,-?\d+)\>, r=(\d+)')
with open(os.path.join(os.getcwd(), r"Advent Of Code 2018", r"d23.txt")) as f:
    for line in f:
        pos, r = rx.match(line.strip()).groups()
        pos = tuple(map(int, pos.split(',')))
        r = int(r)
        sphere = pyrr.sphere.create()
        bots.add_node(pos, radius = r)

# for all bots, calculate how many bots have this one in range
# then check all the points in the radius of the bot with most other bots
# def find_other_bots(bot):
#     x, y, z, _ = bot
#     other_bots_have_this_in_range = 0
#     for bot2 in bots:
#         X, Y, Z, R = bot2
#         if abs(x-X) + abs(y-Y) + abs(z-Z) <= R:
#             other_bots_have_this_in_range += 1
#     return other_bots_have_this_in_range
    
for bot1 in bots.nodes():
    for bot2 in bots.nodes:
        if bot1 != bot2:
            if bot2 not in bots[bot1]:
                x, y, z = bot1
                X, Y, Z = bot2
                distance = abs(x - X) + abs(y - Y) + abs(z - Z)
                if (distance <= bots.nodes[bot1]['radius']):
                    bots.add_edge(bot1, bot2, weight = distance)
                if (distance <= bots.nodes[bot2]['radius']):
                    bots.add_edge(bot2, bot1, weight = distance)
    
print('part 1 done')
# pos=nx.drawing.layout.kamada_kawai_layout(bots)
# pos=nx.drawing.layout.spring_layout(bots)
# nx.draw(bots, with_labels = False, pos=pos, edge_color ='grey')

print(bots.number_of_edges())
import pylab as plt
nodes = nx.draw_networkx_nodes(bots)
# nx.draw(bots)
plt.savefig('graph.png')
plt.close()


# edge_labels = nx.get_edge_attributes(mazex,'doors')
# edge_labels = nx.draw_networkx_edge_labels(mazex, edge_labels=edge_labels, pos=pos)
plt.show() 
