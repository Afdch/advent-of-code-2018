import numpy as np
from datetime import datetime

dt = datetime.now()

serialNo = 8444
size = 300
powerCells = []

for y in range(size):
    row = [0]*size
    for x in range(size):
        rackID = x + 11
        power = (rackID*(y + 1) + serialNo)*rackID
        powerCells.append(int((power%1000)/100) - 5)

pc = np.array(powerCells).reshape(size, size)


def find_max_area(dial):
    max_area_power = 0
    max_area_X, max_area_Y = 0, 0
    max_dial_size = 0
    print(f"size {dial:3} areas")
    for Y in range(0, size + 1 - dial):
        for X in range(0, size + 1 - dial):
            area_power = np.sum(pc[Y:Y + dial, X:X + dial])
            if area_power > max_area_power:
                max_area_power = area_power
                max_area_X = X + 1
                max_area_Y = Y + 1
                max_dial_size = dial
    return max_area_X, max_area_Y, max_dial_size, max_area_power


def part1():
    max_area_X, max_area_Y, max_dial_size, max_area_power = find_max_area(3)
    print(f'{max_area_X},{max_area_Y} at {max_area_power} power')


def part2():
    max_area_power = 0
    max_area_X, max_area_Y = 0, 0
    max_dial_size = 0
    for dial_size in range(1, 300 + 1):
        mx_area_X, mx_area_Y, mx_dial_size, mx_area_power = find_max_area(dial_size)
        if mx_area_power > max_area_power:
            max_area_power = mx_area_power
            max_area_X = mx_area_X
            max_area_Y = mx_area_Y
            max_dial_size = mx_dial_size
    print(f'{max_area_X},{max_area_Y},{max_dial_size} at {max_area_power} power')


part1()
print(datetime.now() - dt)
part2()
print(datetime.now() - dt)
