import re
from PIL import Image

pattern = re.compile(r'position=< ?(\-?\d+),  ?(\-?\d+)> velocity=< ?(\-?\d+),  ?(\-?\d+)>')
# X represents how far left (negative) or right (positive) the point appears,
# while Y represents how far up (negative) or down (positive) the point appears.
points = []

with open("d10.txt") as f:
    for line in f:
        x, y, dx, dy = pattern.match(line.strip()).groups()
        points.append(((int(x), int(y)), (int(dx), int(dy))))


def get_min(points):
    min_x, min_y = points[0][0]
    for (x, y), vels in points:
        min_x = min(x, min_x)
        min_y = min(y, min_y)
    return min_x, min_y


def get_max(points):
    max_x, max_y = points[0][0]
    for (x, y), vels in points:
        max_x = max(x, max_x)
        max_y = max(y, max_y)
    return abs(max_x), abs(max_y)


def get_area(points):
    max_x, max_y = get_max(points)
    min_x, min_y = get_min(points)
    return max_x - min_x, max_y - min_y


def increment(position, incrementation):
    x, y = position
    dx, dy = incrementation
    x += dx
    y += dy
    return x, y


min_neg = get_min(points)
for i in range(len(points)):
    position, vels = points[i]
    points[i] = increment(position, (-min_neg[0], -min_neg[1])), vels


j = 0
height, width = get_area(points)
n_height = 0

while n_height <= height:
    for i in range(len(points)):
        position, vels = points[i]
        points[i] = increment(position, vels), vels
    n_height, n_width = get_area(points)
    if n_height <= height:
        height = n_height
        width = n_width
        j += 1
else:
    for i in range(len(points)):
        position, vels = points[i]
        points[i] = increment(position, (-vels[0], -vels[1])), vels
print(j)

min_neg = get_min(points)
for i in range(len(points)):
    position, vels = points[i]
    points[i] = increment(position, (-min_neg[0], -min_neg[1])), vels
max_pos = get_max(points)

image = Image.new('RGB', (max_pos[0] + 1, max_pos[1] + 1), "black")
pixels = image.load()
for (x, y), vels in points:
    pixels[x, y] = 159, 168, 218
image.show()
