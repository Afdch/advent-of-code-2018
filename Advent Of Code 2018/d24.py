import re
import copy

pattern = re.compile(r'(\d+) units each with (\d+) hit points (.+)? ?with an attack that does (\d+) ([a-z]+) damage at initiative (\d+)')
patt_imm = re.compile(r'.+immune to ([a-z, ]+)')
patt_weak = re.compile(r'.+weak to ([a-z, ]+)')


class Group:
    def __init__(self, id, type, number, hp, immuns, weaks, damage, dmg_type, initiative):
        self.id, self.type, self.number, \
        self.hp, self.immuns, self.weaks, self.damage, \
        self.dmg_type, self.initiative = \
            id, type, number, \
            hp, immuns, weaks, damage, \
            dmg_type, initiative


groupss = []
imms = True
with open('d24.txt') as f:
    u_id = 1
    for line in f:
        if line.strip() == 'Infection:':
            imms = False
            u_id = 1
        elif line.strip() and line.strip() != 'Immune System:':
            u_weaks = []
            u_immuns = []
            u_number, u_hp, u_specifics, u_damage, u_dmg_type, u_initiative = pattern.match(line.strip()).groups()
            if u_specifics is not None:
                u_immuns = patt_imm.search(u_specifics).group(1).split(', ') if patt_imm.search(u_specifics) else []
                u_weaks = patt_weak.search(u_specifics).group(1).split(', ') if patt_weak.search(u_specifics) else []
            u_number, u_hp, u_damage, u_initiative = tuple(map(int, (u_number, u_hp, u_damage, u_initiative)))
            if imms:
                u_type = 'Imm'
            else:
                u_type = 'Inf'
            
            groupss.append(Group(u_id, u_type, u_number, u_hp, u_immuns, u_weaks, u_damage, u_dmg_type, u_initiative))
            print(u_id, u_type, u_number * u_damage, u_number, u_hp, u_immuns, u_weaks, u_damage, u_dmg_type, u_initiative, '\n')
            u_id += 1
            

"""Each attacker also has an effective power:
the number of units in that attacker
multiplied by their attack damage."""
boost = 36
while True:
    groups = copy.deepcopy(groupss)
    for group in groups:
        if group.type == 'Imm':
            group.damage += boost
    while True:
        """Combat phase 1: target selection
        each attacker attempts to choose one target.
        In decreasing order of effective power, groups choose their targets;
        in a tie, the attacker with the higher initiative chooses first.
        """
        groups.sort(key = lambda x: -x.initiative)
        groups.sort(key = lambda x: -x.number * x.damage)
        selected = []
        targets = []
        """
        The attacking attacker chooses to target the attacker in the enemy army
        to which it would deal the most damage
        (after accounting for weaknesses and immunities,
        but not accounting for whether the defending attacker has enough units
        to actually receive all of that damage).
        """
        for attacker in groups:
            valid_targets = [t for t in groups if t.type != attacker.type and t not in selected and attacker.dmg_type not in t.immuns]
            # for target in valid_targets:
                # print(f"Group {attacker.type} {attacker.id} would deal {target.type} {target.id} "
                #       f"est {attacker.damage*attacker.number*(0 if attacker.dmg_type in target.immuns else 1)*(2 if attacker.dmg_type in target.weaks else 1)} damage ")
    
            if len(valid_targets) > 0:
                valid_targets.sort(key = lambda u: -u.initiative)
                valid_targets.sort(key = lambda u: -u.damage * u.number)
                valid_targets.sort(key = lambda u:
                                   - attacker.number*attacker.damage
                                   * (2 if attacker.dmg_type in u.weaks else 1))
                target = valid_targets[0]
                selected.append(target)
                targets.append((attacker, target))
                # print(f"Group {attacker.type} {attacker.id} would deal {target.type} {target.id} "
                #       f"est {attacker.damage * attacker.number * (0 if attacker.dmg_type in target.immuns else 1) * (2 if attacker.dmg_type in target.weaks else 1)} damage ")
        """During the attacking phase, each attacker deals damage
        to the target it selected, if any.
        Groups attack in decreasing order of initiative,
        regardless of whether they are part of the infection or the immune system.
        (If a attacker contains no units, it cannot attack.)"""
        
        targets.sort(key = lambda init: -init[0].initiative)
        
    
        
        """The defending attacker only loses whole units from damage;
        damage is always dealt in such a way that
        it kills the most units possible, and any remaining damage
        to a unit that does not immediately kill it is ignored."""
        
        dead_groups = []
        
        for dealer, target in targets:
            if dealer not in dead_groups:
                """The damage an attacking attacker deals to a defending attacker
                depends on the attacking attacker's attack type
                and the defending attacker's immunities and weaknesses.
                default, an attacking attacker would deal damage
                equal to its effective power to the defending attacker.
                However, if the defending attacker is immune
                to the attacking attacker's attack type,
                the defending attacker instead takes no damage;
                if the defending attacker is weak to the attacking attacker's attack type,
                the defending attacker instead takes double damage."""
                damage = dealer.number * dealer.damage * (0 if dealer.dmg_type in target.immuns else 1) * (2 if dealer.dmg_type in target.weaks else 1)
                ku = damage // target.hp
                # print(f'{dealer.type} {dealer.id} attacks {target.type} {target.id} and kills {min(ku, target.number)} units ')
                target.number -= ku
                if target.number <= 0:
                    dead_groups.append(target)
        """After the fight is over, if both armies still contain units, a new fight begins;
        combat only ends once one army has lost all of its units."""
        
        groups = [group for group in groups if group not in dead_groups]
        inf = [group for group in groups if group.type == 'Inf']
        imm = [group for group in groups if group.type == 'Imm']
        
        # print(" ")
        
        if len(inf) == 0 or len(imm) == 0:
            break

    print(f'Boost {boost}:\t{sum([group.number for group in groups])}')
    if len([group for group in groups if group.type == 'Inf']) == 0:
        print("Finally! =========================")
        print(sum([group.number for group in groups]))
        break

    boost += 1
# print(sum([group.number for group in groups]))
# 18687 is too low