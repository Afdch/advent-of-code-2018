import re
import numpy as np
from PIL import ImageDraw, Image

pattern_y = re.compile(r'y=(\d+\.*\d*)')
pattern_x = re.compile(r'x=(\d+\.*\d*)')

max_x = 500
min_x = 500
max_y = 0
min_y = 100
veins = []
with open('d17.txt') as f:
    for line in f:
        yc = pattern_y.search(line.strip()).group(1)
        xc = pattern_x.search(line.strip()).group(1)
        if ".." in yc:
            yc = [int(y) for y in yc.split("..")]
            for y in yc:
                if y > max_y:
                    max_y = y
                if y < min_y:
                    min_y = y
            xc = int(xc)
            if xc > max_x:
                max_x = xc
            if xc < min_x:
                min_x = xc
        else:
            xc = [int(x) for x in xc.split("..")]
            for x in xc:
                if x > max_x:
                    max_x = x
                if x < min_x:
                    min_x = x
            yc = int(yc)
            if yc > max_y:
                max_y = yc
            if yc < min_y:
                min_y = yc
        
        veins.append((xc, yc))

# for v in veins:
#     print(v)
# print(min_x, max_x, max_y)


X = max_x - min_x + 5
Y = max_y + 1
slice = np.zeros((Y, X), dtype = int)
slice = np.matrix(slice)

for v in veins:
    if isinstance(v[1], int):  # if y is int
        for x in range(v[0][0] - min_x, v[0][1] - min_x + 1):
            slice[v[1], x + 2] = 1  #
    else:  # else x is int
        for y in range(v[1][0], v[1][1] + 1):
            slice[y, v[0] - min_x + 2] = 1

water_source = (0, 500 - min_x + 1)
slice[water_source] = 8

images = []


def img_draw(save = False):
    image = Image.new('RGB', (X, Y), "black")
    pixels = image.load()
    for x in range(X):
        for y in range(Y):
            if slice[y, x] == 1:
                pixels[x, y] = 255, 255, 255
            elif slice[y, x] == 8:
                pixels[x, y] = 0, 0, 255
            elif slice[y, x] == 5:
                pixels[x, y] = 0, 255, 255
    
    # image.show()
    scale_factor = 8
    image = Image.Image.resize(image, (X*scale_factor, Y*scale_factor))
    if save:
        image.show()
    else:
        images.append(image)

water_fall = [8]

something_has_changed_this_turn = True
while something_has_changed_this_turn:
    
    something_has_changed_this_turn = False
    # all water falls:
    wf = np.where(slice == 8)
    for i in reversed(range(len(wf[0]))):
        # print("\n")
        # print(slice)
        if wf[0][i] < max_y:
            # if the tile below is falling water
            if slice[wf[0][i] + 1, wf[1][i]] == 8:
                # do literally nothing
                continue
            
            # if the tile below is sand
            if slice[wf[0][i] + 1, wf[1][i]] == 0:
                # water can fall
                slice[wf[0][i] + 1, wf[1][i]] = 8
                something_has_changed_this_turn = True
                continue
            
            
            # else if the tile below is a clay (or a water stand)
            elif slice[wf[0][i] + 1, wf[1][i]] in [1, 5]:
                
                if slice[wf[0][i], wf[1][i] + 1] in [8, 9] and slice[wf[0][i], wf[1][i] - 1] in [8, 9]:
                    continue
                
                # watching tiles to the right first
                if slice[wf[0][i], wf[1][i] + 1] == 0:
                    slice[wf[0][i], wf[1][i] + 1] = 8
                    something_has_changed_this_turn = True
                elif slice[wf[0][i], wf[1][i] + 1] == 1:
                    # if we hit the wall
                    current_x = wf[1][i]
                    ws_start = current_x
                    while True:
                        if slice[wf[0][i], current_x] != 8:
                            slice[wf[0][i], current_x] = 8
                            something_has_changed_this_turn = True
                        if slice[wf[0][i], current_x - 1] == 1:
                            # we hit another wall todo
                            # print("\n")
                            # print(slice)
                            something_has_changed_this_turn = True
                            for x in range(min(ws_start, current_x), max(current_x, ws_start) + 1):
                                slice[wf[0][i], x] = 5
                            # print("\n")
                            # print(slice)
                            # img_draw(save = True)
                            break
                        elif slice[wf[0][i] + 1, current_x] in [0, 8]:
                            if wf[0][i] + 2 < max_y:
                                slice[wf[0][i] + 1, current_x] = 8
                                # something_has_changed_this_turn = True
                            # water can fall down
                            break
                        current_x -= 1
                # then turn to the left:
                if slice[wf[0][i], wf[1][i] - 1] == 0:
                    something_has_changed_this_turn = True
                    slice[wf[0][i], wf[1][i] - 1] = 8
                elif slice[wf[0][i], wf[1][i] - 1] == 1:
                    # if we hit the wall
                    current_x = wf[1][i]
                    ws_start = current_x
                    while True:
                        if slice[wf[0][i], current_x] != 8:
                            slice[wf[0][i], current_x] = 8
                            something_has_changed_this_turn = True
                        if slice[wf[0][i], current_x + 1] == 1:
                            # we hit another wall
                            for x in range(min(ws_start, current_x), max(current_x, ws_start) + 1):
                                slice[wf[0][i], x] = 5
                            # img_draw(save = True)
                            break
                        elif slice[wf[0][i] + 1, current_x] in [0, 8]:
                            if wf[0][i] + 2 < max_y:
                                slice[wf[0][i] + 1, current_x] = 8
                                # something_has_changed_this_turn = True
                            # water can fall down
                            break
                        current_x += 1
            # print('stop')
    # print('\n')
    # print(slice)
    
    # img_draw(save = True)
    
    water_tiles_8 = len(np.where(slice == 8)[0])
    water_tiles = len(np.where(slice == 5)[0]) + water_tiles_8 + \
                  len(np.where(slice == 9)[0]) + len(np.where(slice == 4)[0])

water_tiles_ret = len(np.where(slice[min_y:, :] == 5)[0])
water_tiles_fal = len(np.where(slice[min_y:, :] == 8)[0])
print(water_tiles_ret)
print(water_tiles_ret + water_tiles_fal)

try:
    images[0].save('anim.gif', save_all = True, append_images = images[1:], duration = 10, loop = 0)
except MemoryError:
    img_draw(save = True)
except IndexError:
    img_draw(save = True)

