twoLetterCount = 0
threeLetterCount = 0
with open("d2.txt") as f:
	for line in f:
		#boxIds.append(line.strip())
		twoLetterLine = False
		threeLetterLine = False
		for letter in line:
			if line.count(letter) == 2:
				twoLetterLine = True
			if line.count(letter) == 3:
				threeLetterLine = True
		if twoLetterLine:
		   twoLetterCount += 1
		if threeLetterLine:
		   threeLetterCount += 1
print(twoLetterCount, threeLetterCount, twoLetterCount * threeLetterCount)


# (count 2letter) x (count 3letter)