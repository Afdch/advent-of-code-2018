import re

coordinates = []
pattern = re.compile(r'(\d+), (\d+)')

with open("d6.txt") as f:
    for line in f:
        coordinates.append((int(pattern.match(line.strip()).group(1)), int(pattern.match(line.strip()).group(2))))

areaSize = 350
area = [[0 for i in range(areaSize)] for i in range(areaSize)]
ar = 0
for lineInd in range(areaSize):
    for squareInd in range(areaSize):
        distance = 0
        for coordinate in coordinates:
            distance += (abs(coordinate[0] - squareInd) + abs(coordinate[1] - lineInd))
        if distance < 10000:
            ar += 1
print(ar)
