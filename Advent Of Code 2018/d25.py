points = []
with open('d25.txt') as f:
    for line in f:
        points.append(tuple(map(int, line.strip().split(','))))
  
        
def dist(p1, p2):
    a1, b1, c1, d1 = p1
    a2, b2, c2, d2 = p2
    return abs(a1-a2) + abs(b1-b2) + abs(c1-c2) + abs(d1-d2) <= 3


# if any(dist(point, p2) for p2 in constellation if p2 is not point):
constellations = []

# while len(points) > 0:
#     constellation = set()
#     pz = points[0]
#     constellation.add(pz)
#     points.sort(key = lambda x:
#                 abs(x[0] - pz[0]) +
#                 abs(x[1] - pz[1]) +
#                 abs(x[2] - pz[2]) +
#                 abs(x[3] - pz[3]))
#
#     for point in points:
#         [constellation.add(p) for p in points
#          if any(dist(p, p3) for p3 in constellation)
#          and p is not point
#          and p not in constellation]
#     print(len(constellation))
#     constellations.append(constellation)
#     points = [p for p in points if p not in constellation]

while len(points) > 0:
    print('Next constellation:')
    cn = []
    cn.append(points[0])
    for point in cn:
        [cn.append(p) for p in points
         if p not in cn
         and any(dist(p, p1) for p1 in cn)]
    constellations.append(cn)
    points = [p for p in points if p not in cn]
    print(len(cn))
         

print(len(constellations))

