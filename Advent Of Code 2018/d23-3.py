from typing import overload
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import re
import os

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

def drawSphere(xCenter, yCenter, zCenter, r, color = '#ffaa66', alpha = 0.1):
    #draw sphere
    global ax
    u, v = np.mgrid[0:2*np.pi:10j, 0:np.pi:10j]
    x=np.cos(u)*np.sin(v)
    y=np.sin(u)*np.sin(v)
    z=np.cos(v)
    # shift and scale sphere
    x = r*x + xCenter
    y = r*y + yCenter
    z = r*z + zCenter
    ax.plot_wireframe(x, y, z, color=color, alpha = alpha)
    
bots = []

rx = re.compile(r'pos=\<(-?\d+,-?\d+,-?\d+)\>, r=(\d+)')
with open(os.path.join(os.getcwd(), r"d23.txt")) as f:
    for line in f:
        pos, r = rx.match(line.strip()).groups()
        x, y, z = tuple(map(int, pos.split(',')))
        r = int(r)
        bots.append((x, y, z, r))

all_X, all_Y, all_Z, all_R = zip(*bots)
all_X = sorted(list(all_X))
all_Y = sorted(list(all_Y))
all_Z = sorted(list(all_Z))

max_X = max([abs(x) for x in all_X])
max_Y = max([abs(x) for x in all_Y])
max_Z = max([abs(x) for x in all_Z])
Max_R = max(all_R)

Boundary = max(max_X, max_Y, max_Z) + Max_R

def bot_is_in_range(bot, point):
    x1, y1, z1, r = bot
    x2, y2, z2 = point
    dist = abs(x1 - x2) + abs(y1 - y2) + abs(z1 - z2)
    return dist <= r

def check_points(point, r) -> int:
    x, y, z = point
    corners = [
        x + r, y + r, z + r,
        x - r, y + r, z + r,
        x + r, y - r, z + r,
        x - r, y - r, z + r,
        x + r, y + r, z - r,
        x - r, y + r, z - r,
        x + r, y - r, z - r,
        x - r, y - r, z - r,
    ]
    bots_in_range = {}
    for point in corners:
        bots_in_range[point] = sum([1 for bot in bots if bot_is_in_range(bot, point)])
                

# region plotting and scheming

for x, y, z, r in bots:    
    # drawSphere(x, y, z, r)
    ax.plot(x, y, 'r+', zdir='z', zs=max_Z+Max_R)
    ax.plot(x, z, 'g+', zdir='y', zs=max_Y+Max_R)
    ax.plot(y, z, 'b+', zdir='x', zs=max_X+Max_R)

# calculate the centre of the mass of all bots
import math
mean_X, mean_Y, mean_Z = [sum(wtf) / sum(all_R) for wtf in zip(*[(x*r, y*r, z*r) for (x, y, z, r) in bots])]
# mean_X = math.sqrt(sum() / len(all_X))
# mean_Y = math.sqrt(sum([a**2 for a in all_Y]) / len(all_Y))
# mean_Z = math.sqrt(sum([a**2 for a in all_Z]) / len(all_Z))
ax.plot(mean_X, mean_Y, mean_Z, 'k^')

print(mean_X, mean_Y, mean_Z)
# plt.show()

# endregion

def bots_in_range(point) -> int:
    return sum([1 for bot in bots if bot_is_in_range(bot, point)])
        

def find_initial_bot() -> tuple():
    # return ([(bots_in_range((x, y, z)), (x, y, z)) for x in all_X for y in all_Y for z in all_Z].sorted())[-1]
    global all_X, all_Y, all_Z
    
    list_ranges = []
    for x, y, z, r in bots:
        list_ranges.append((bots_in_range((x, y, z)), (x, y, z)))
    list_ranges.sort()
    max_B, (x, y, z) = list_ranges[-1]
    list_ranges = [(am, coord) for am, coord in list_ranges if am == max_B]
    sX = all_X.index(x)
    sY = all_Y.index(y)
    sZ = all_Z.index(z)
    
    all_X = all_X[sX - 30:sX + 5]
    all_Y = all_Y[sY - 30:sY + 5]
    all_Z = all_Z[sZ - 30:sZ + 5]
    
    
    for x in all_X:
        for y in all_Y:
            for z in all_Z:
                list_ranges.append(a:= (bots_in_range((x, y, z)), (x, y, z)))
            print(len(list_ranges), end = "\r", flush = True)
    print("\n")
    return sorted(list_ranges)

list_of_bots = find_initial_bot()

bots_r, starting_point = list_of_bots[-1]
from queue import PriorityQueue

def manh(point):
    x, y, z = point
    return abs(x) + abs(y) + abs(z)

def neighbours(point):
    x, y, z = point
    return [(x+dx, y+dy, z+dz) for dx in range(-1, 2) for dy in range(-1, 2) for dz in range(-1, 2) if not dx == dy == dz == 0]


frontier = []
frontier.append((manh(starting_point), bots_r, starting_point))
visited = {starting_point:(bots_r, manh(starting_point))}
max_bots = bots_r
mnh = manh(starting_point)

while len(frontier) > 0:
    manh_dist, bots_r, point = frontier.pop(0)
    visited[point] = (bots_r, manh_dist)
    print(manh_dist, bots_r, end = '\r', flush= True)
    mn= manh(point)
    if bots_r < max_bots:
        continue
    elif bots_r > max_bots:
        max_bots = bots_r
        if mn < mnh:
            mnh = mn
    elif bots_r == max_bots:
        #TODO check manh dist?
        if mn > mnh:
            continue

    for neighbor in neighbours(point):
        if neighbor in visited.keys():
            continue
        if (bb:=bots_in_range(neighbor)) < max_bots:
            continue
        elif bb > max_bots:
            max_bots = bb            
        frontier.append((manh(neighbor), bb, neighbor))
    frontier = [(a, b, c) for a, b, c in frontier if b == max_bots]
    frontier.sort()
    
    
print('\n', max_bots, mnh)