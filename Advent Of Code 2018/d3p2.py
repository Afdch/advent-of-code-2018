import re
claims = []
pattern = re.compile(r'#(?P<claimID>\d+)\D+(?P<x>\d+)\D(?P<y>\d+):\s(?P<width>\d+)x(?P<height>\d+)')
with open("d3.txt") as f:
	for line in f:	
		claims.append(pattern.match(line.strip()).groups())
fabric = [[[0, []] for x in range(1000)] for y in range(1000)]
for claim in claims:
	for x in range(int(claim[1]), int(claim[1]) + int(claim[3])):
		for y in range(int(claim[2]), int(claim[2]) + int(claim[4])):
			fabric[x][y][0] += 1
			fabric[x][y][1].append(int(claim[0]))
inches = 0
claims2 = []
for claim in claims:
	claims2.append(int(claim[0]))
for row in fabric:
	for inch in row:
		if inch[0] > 1:
			inches += 1
			for claim in inch[1]:
				if claim in claims2:
					claims2.remove(claim)
print(inches)
print(claims2)