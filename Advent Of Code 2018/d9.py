import re

""""
        THIS IS A REALLY EMBARRASSING CODE,
        FOR A PROPER SOLUTION LOOK NINE.PY
        this took literally 2h 45m
        to solve part 2 of the task
                                        """
from datetime import datetime

pattern = re.compile(r'(\d+) .+ (\d+) .+')
inp = '452 players; last marble is worth 70784 points'

players = {}
game_set = []
dt = datetime.now()


def result(inp):
    player_count, last_worth = pattern.match(inp).groups()
    player_count = int(player_count)
    last_worth = int(last_worth)
    players.update({player: 0 for player in range(player_count)})
    
    new_last_worth = last_worth
    
    dt = datetime.now()
    # high_score = game(player_count, new_last_worth)
    # print('Old: ', datetime.now()-dt, high_score)
    
    game_set.clear()
    players.update({player: 0 for player in range(player_count)})
    dt = datetime.now()
    high_score = game(player_count, new_last_worth)
    print('New: ', datetime.now() - dt, high_score)
    
    # print(players)
    return high_score


def game2(player_count, last_marble):
    index = 0
    percent = last_marble/10
    a, b, c, t = [datetime.now()]*4
    for current_marble in range(last_marble):
        dt1 = datetime.now()
        for j in range(22):
            game_set.insert(index, current_marble)
            index = index + 2 if index < len(game_set) - 1 else 1
            current_marble += 1
        dt2 = datetime.now()
        index = index + len(game_set) - 7 if index < 7 else index - 7
        removed_marble = game_set.pop(index)
        current_player = current_marble%player_count
        players.update({current_player: current_marble + removed_marble + players.get(current_player)})
        current_marble += 1
        dt3 = datetime.now()
        if current_marble > percent:
            percent += last_marble/10
            print(datetime.now() - dt, "{0:>9,d}\t{1:>10,d}".format(current_marble, players.get(max(players, key = players.get))).replace(',', ' '))
        dt4 = datetime.now()
        
        a += dt2 - dt1
        b += dt3 - dt2
        c += dt4 - dt3
    
    print(a - t, b - t, c - t)
    high_score = players.get(max(players, key = players.get))
    return high_score


def game(player_count, last_marble):
    current_marble = 0
    index = 0
    for current_marble in range(last_marble):
        # print(current_player, game_set)
        # print(index, current_marble, current_player)
        # print(players.get(max(players, key=players.get)))
        if index - 9 < 0:
            removed_marble = game_set[index + len(game_set) - 9] if current_marble > 9 and current_marble%23 == 0 else None
        else:
            removed_marble = game_set[index - 9] if current_marble > 9 and current_marble%23 == 0 else None
        if removed_marble is not None:
            # print(index, current_marble, current_player, removed_marble)
            current_player = current_marble%player_count
            players.update({current_player: current_marble + removed_marble + players.get(current_player)})
            if index - 9 < 0:
                index = index + len(game_set) - 9
            else:
                index -= 9
            game_set.pop(index)
        else:
            if index + 1 > len(game_set) + 1:
                index = 1
            game_set.insert(index, current_marble)
        index += 2
    # if current_marble + removed_marble == last_marble:
    # if current_marble % 10000 == 0:
    #     print(datetime.now()-dt, "{0:,d}\t{1:,d}".format(current_marble, players.get(max(players, key = players.get))).replace(',', ' '), players)
    return players.get(max(players, key = players.get))


result(inp)
