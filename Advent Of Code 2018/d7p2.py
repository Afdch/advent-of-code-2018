import re
from datetime import datetime

dt = datetime.now()

instructions = []
pattern = re.compile(r'Step ([A-Z]) must be finished before step ([A-Z]) can begin.')

with open("d7.txt") as f:
    for line in f:
        instructions.append(pattern.match(line.strip()).groups())

instructions = sorted(instructions)

completedInstructions = []
allInstructions = ['A', 'B', 'C', 'D', 'E', 'F', 'G',
                   'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                   'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
currentInstructions = [I for I in allInstructions if I not in [F for (S, F) in instructions]]


# Step S must be finished before step F can begin
def get_instruction(instr):
    return [F for (S, F) in instructions if S == instr]


def prerequirement(instr):
    return [I for I in allInstructions if I in [F for (F, S) in instructions if S == instr] if I not in completedInstructions]


currentSecond = 0
freeWorkers = 5
# inWork = ('instruction', 'startedAt')
inWork = []
# for instruction in allInstructions:
#     print(instruction, prerequirement(instruction), get_instruction(instruction))

relevant_seconds = []

while len(currentInstructions) > 0 or len(inWork) > 0:
    for workInstruction in inWork:
        if workInstruction[1] + allInstructions.index(workInstruction[0]) + 60 < currentSecond:
            # print(f'{currentSecond}: Instruction {workInstruction[0]} completed. Free workers: {freeWorkers + 1}')
            inWork.remove(workInstruction)
            completedInstructions.append(workInstruction[0])
            freeWorkers += 1
            for nextInstruction in get_instruction(workInstruction[0]):
                if len(prerequirement(nextInstruction)) == 0:
                    currentInstructions.append(nextInstruction)
    inWork.sort()  #
    while freeWorkers > 0 and len(currentInstructions) > 0:
        currentInstructions.sort()
        currentInstruction = currentInstructions.pop(0)
        freeWorkers -= 1
        inWork.append((currentInstruction, currentSecond))
        relevant_seconds.append(currentSecond + ord(currentInstruction) - 5)
        # print(f"{currentSecond}: instruction {currentInstruction} started. Free workers: {freeWorkers}. "
        #       f"Instructions in work: {len(inWork)}: {[x for (x, time) in inWork]}")
    if len(inWork) > 0:
        currentSecond = relevant_seconds.pop(0) + 1
        # print(currentSecond)

instructionString = ''
for instruction in completedInstructions:
    instructionString += instruction
print(instructionString)
print(currentSecond)
print(datetime.now() - dt)
