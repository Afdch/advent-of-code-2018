from datetime import datetime
dt = datetime.now()

inp = []
with open("d8.txt") as f:
    for line in f:
        inp += [int(x) for x in line.split(' ')]


# converts input to ([children], [metadata])
def convert_input_first(inpt, mode = 'external'):
    
    children_count = inpt.pop(0)
    metadata_count = inpt.pop(0)
    children = []
    for i in range(children_count):
        child, inpt = convert_input(inpt, 'internal')
        children.append(child)
    metadata = inpt[:metadata_count]
    inpt = inpt[metadata_count:]
    return ((children, metadata), inpt) if mode == 'internal' else (children, metadata)


def convert_input(inpt, index = 0, mode = 'external'):
    """testing the exact logic as above using index instead"""
    children_count = inpt[index]
    metadata_count = inpt[index + 1]
    index += 2
    children = []
    for i in range(children_count):
        child, index = convert_input(inpt, index, 'internal')
        children.append(child)
    metadata = inpt[index:index + metadata_count]
    index += metadata_count
    return ((children, metadata), index) if mode == 'internal' else (children, metadata)
    

def get_meta_count(data):
    metavalue = 0
    for child in data[0]:
        metavalue += get_meta_count(child)
    return metavalue + sum(data[1])


def get_node_value(data):
    node_value = 0
    if len(data[0]) == 0:
        return sum(data[1])
    else:
        for index in data[1]:
            if index <= len(data[0]):
                node_value += get_node_value(data[0][index - 1])
        return node_value


# just some fun :D
def print_nodes(data, depth = 0):
    string = '| '*depth
    if len(data[0]) > 0:
        print(f'{string}V Children number: {len(data[0])}:')
    else:
        print(f'{string}v ')
    for child in data[0]:
        print_nodes(child, depth + 1)
    print(f'{string}> Metadata: {data[1]}')


print('|read took \t\t|', datetime.now() - dt)
dt2 = datetime.now()

result = convert_input(inp)

print('|converting\t\t|')
print('|operation took\t|', datetime.now() - dt2)
print('|since start \t|', datetime.now() - dt)
dt2 = datetime.now()

print(f'|Meta {get_meta_count(result)}\t\t|')

print('|operation took\t|', datetime.now() - dt2)
print('|since start \t|', datetime.now() - dt)
dt2 = datetime.now()

print(f'|Node {get_node_value(result)}\t\t|')

print('|operation took\t|', datetime.now() - dt2)
print('|since start \t|', datetime.now() - dt)
print_nodes(result)
print('|operation took\t|', datetime.now() - dt2)
print('|since start \t|', datetime.now() - dt)
