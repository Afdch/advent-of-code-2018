from operator import attrgetter, itemgetter


def heuristic(a, b): return abs(a[0] - b[0]) + abs(a[1] - b[1])


class Graph:
    def __init__(self):
        self.grid = {}
    
    def add(self, position, value):
        """value: 0 for cavern,  1 for walls,
                  2 for Goblins, 3 for elves    """
        self.grid[position] = value

    def neighbours(self, position):
        neigh = []
        x, y = position
        condition = self.grid[position] != 1  # if unit_search else self.grid[position] == 0
        if condition:
            # just filters out the walls, other obstacles filtered in find_path
            if self.grid[(x, y - 1)] != 1:
                neigh.append((x, y - 1))
            if self.grid[(x, y + 1)] != 1:
                neigh.append((x, y + 1))
            if self.grid[(x - 1, y)] != 1:
                neigh.append((x - 1, y))
            if self.grid[(x + 1, y)] != 1:
                neigh.append((x + 1, y))
        return neigh

    def find_path(self, _target, _source, mode = 'A*'):
        """
        
        :param _target: the location of a target place
        :param _source: the location of a caller
        :param mode:
        :return:
        """
        frontier = [_target]
        came_from = {}
        cost_so_far = {}
        came_from[_target] = None
        cost_so_far[_target] = 0
        valid = False
        if map.grid[_target] != 0:
            return False
        while len(frontier) > 0:
            frontier.sort(key = itemgetter(0))
            frontier.sort(key = itemgetter(1))
            current = frontier.pop(0)
            
            # Early exit
            # if current == _source and mode == 'A*':
            #     return came_from
        
            # flag valid
            if current == _source:
                valid = True
        
            # todo do I need to sort this too?
            ne = self.neighbours(current)
            # ne.sort(key = itemgetter(0))
            # ne.sort(key = itemgetter(1))
            for next in ne:
                # todo check for obstacles in next position
                if map.grid[next] != 0:
                    if next != _source:
                        continue

                new_cost = cost_so_far[current] + 1  # graph.cost(current, next)

                if next not in came_from or new_cost < cost_so_far[next]:
                    cost_so_far[next] = new_cost
                    if mode == 'A*':
                        priority = new_cost + heuristic(_source, next)
                    else:  # elif mode == 'Dijkstra'
                        priority = new_cost
    
                    frontier.append(next)
                    came_from[next] = current
        if valid:
            return came_from, cost_so_far
        else:
            return False
    
    def print_map(self):
        map_items = self.grid.items()
        max_y = max([_y for (_x, _y), val in map_items])
        max_x = max([_x for (_x, _y), val in map_items])
        print(' ', ''.join([str(k) for k in range(max_x + 1)]))
        for j in range(max_y + 1):
            string = ''
            for i in range(max_x + 1):
                string += str(map.grid[(i, j)])
            print(j, string.replace('1', '#').replace('0', '.').replace('2', 'G').replace('3', 'E'))


class Unit:
    def __init__(self, x, y, type, id):
        self.id = id
        self.health = 200
        self.x = x
        self.y = y
        self.type = type
    
    def move(self, destination_path):
        destination, path, cost = destination_path
        map.add((self.x, self.y), 0)
        dest = path.get((self.x, self.y))
        if dest is not None:
            self.x, self.y = dest
        map.add((self.x, self.y), 3 if self.type == 'E' else 2)



units = []
map = Graph()
with open('d15.txt') as f:
    y = 0
    id = 0
    for line in f:
        x = 0
        for ch in line.strip():
            if ch == "#":
                v = 1
            elif ch == "G":
                v = 2
            elif ch == "E":
                v = 3
            else:
                v = 0
            map.add((x, y), v)
            if ch == "G":
                units.append(Unit(x, y, "G", id))
                id += 1
            elif ch == "E":
                units.append(Unit(x, y, "E", id))
                id += 1
            x += 1
        y += 1

print('read done!')
map.print_map()

elves = [unit for unit in units if unit.type == "E"]
goblins = [unit for unit in units if unit.type == "G"]
round_count = 0
while len(elves) > 0 and len(goblins) > 0:
    
    print(f"==========================\n"
          f"||round {round_count + 1:^5}  started! ||\n"
          f"==========================\n")
    killed_units = []
    units.sort(key = attrgetter("x"))
    units.sort(key = attrgetter("y"))
    unit_locs = [(unit.x, unit.y) for unit in units]
    for unit in units:
        if unit not in killed_units:
            # =======movement check be here ===============
            neighbouring_enemies = [enemy for enemy in units if (enemy.x, enemy.y) in map.neighbours((unit.x, unit.y))
                                    and unit.type != enemy.type]
            combat_string = f"\nTurn {round_count + 1} combat phase:\nUnit {unit.type} id {unit.id} at {unit.x}x{unit.y} has "
            if len(neighbouring_enemies) == 0:
                in_range = []
                reachable = []
                nearest = []
            
                """ Check every enemy, if it has empty surrounding areas,
                    add them to in_range list   """
                for target in units:
                    if target != unit:
                        if target.type != unit.type:
                            tx, ty = target.x, target.y
                            for loc in map.neighbours((tx, ty)):
                                if loc not in unit_locs:
                                    in_range.append(loc)
            
                # print(f'{(unit.x, unit.y)} unit {unit.type} has cells {in_range} around all enemies')
                in_range.sort(key = itemgetter(0))
                in_range.sort(key = itemgetter(1))
                """ From those locations, select those are reachable """
                for place in in_range:
                
                    # if map.grid[place] == 0:
                    result = map.find_path(place, (unit.x, unit.y))
                    # print(f'{place} is reachable? : {path}')
                    if result is not False:
                        path, cost = result
                        dist = cost[(unit.x, unit.y)]
                        reachable.append((place, path, dist))
            
                """ If there are reachable locations, find those with shortest path to them """
                if len(reachable) > 0:
                    reachable.sort(key = itemgetter(2))
                    min_dist = min([dist for place, path, dist in reachable])
                    nearest = [place for place, path, dist in reachable if dist == min_dist]

                
                    if len(nearest) >= 1:
                        nearest.sort(key = itemgetter(0))
                        nearest.sort(key = itemgetter(1))
                        chosen = nearest[0]
                    
                        chosen_path = [(place, path, cost) for place, path, cost in reachable if place == chosen][0]
                        # print(f'{(unit.x, unit.y)} unit {unit.type} has {nearest} nearest cells,'
                        #       f'from which {chosen} is chosen\nPath to destination:{chosen_path}')
                    
                        """ Move unit 1 step towards the chosen cell """
                        unit.move(chosen_path)  # todo
            map.print_map()
            # ========movement logic end now===============

            # ========COMBAT LOGIC END HERE================
        
            neighbouring_enemies = [enemy for enemy in units if (enemy.x, enemy.y) in map.neighbours((unit.x, unit.y))
                                    and unit.type != enemy.type]
            combat_string = f"\nTurn {round_count + 1} combat phase:\nUnit {unit.type} id {unit.id} at {unit.x}x{unit.y} has "
            if len(neighbouring_enemies) > 0:
                combat_string += f"{len(neighbouring_enemies)} surrounding enemies:\n"
                neighbouring_enemies.sort(key = attrgetter("x"))
                neighbouring_enemies.sort(key = attrgetter("y"))
                neighbouring_enemies.sort(key = attrgetter("health"))
            
                combat_string += f"ids: {[en.id for en in neighbouring_enemies]}"
                enemy = neighbouring_enemies[0]
                combat_string += f"\nAttacking enemy {enemy.type} id {enemy.id} at {enemy.x}x{enemy.y}: " \
                    f"Enemy HP = {enemy.health} -> {enemy.health - 3} "

                enemy.health -= 3
                if enemy.health <= 0:
                    combat_string += f"\nEnemy {enemy.id} has been killed!!!"
                    map.add((enemy.x, enemy.y), 0)
                    killed_units.append(enemy)
        
            else:
                combat_string += "no surrounding enemies."
        
            print(combat_string)

            # ========COMBAT LOGIC END HERE================
    for unit in killed_units:
        if unit in units:
            units.remove(unit)
    
    for unit in units:
        print(f"{unit.type}{unit.id}|{unit.x}x{unit.y} : {unit.health:>3}")
    print(f"==========================\n"
          f"||All unites have moved!||\n"
          f"||Round {round_count + 1:^5} has ended!||\n"
          f"==========================\n\n\n")
    
    elves = [unit for unit in units if unit.type == "E"]
    goblins = [unit for unit in units if unit.type == "G"]
    round_count += 1
print(f"After {round_count} rounds team {units[0].type} has won! \n"
      f"score = {round_count } * {sum([unit.health for unit in units])} ="
      f" {round_count*sum([unit.health for unit in units])} ")
