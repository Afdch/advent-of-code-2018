from operator import attrgetter

track = []
carts = []
with open('d13.txt') as f:
    for line in f:
        track.append(line)

direction = {
    'n': (-1, 0),
    's': (1, 0),
    'e': (0, 1),
    'w': (0, -1),
}


class Cart:
    def __init__(self, Xx, Yy, direction, maze, id, turn_count = 0):
        self.X = Xx
        self.Y = Yy
        self.id = id
        self.direction = direction
        self.turn_count = turn_count
        self.maze = maze
    
    def move(self):
        x, y = self.X, self.Y
        
        if self.maze[y][x] == '+':
            if self.turn_count%3 == 0:
                self.turn_count += 1
                # print(f'cart {self.id} has turned left')
                if self.direction == 'n':
                    self.direction = 'w'
                elif self.direction == 'w':
                    self.direction = 's'
                elif self.direction == 's':
                    self.direction = 'e'
                elif self.direction == 'e':
                    self.direction = 'n'
            elif self.turn_count%3 == 1:
                self.turn_count += 1
                # print(f'cart {self.id} has not turned')
                pass
            elif self.turn_count%3 == 2:
                self.turn_count += 1
                # print(f'cart {self.id} has turned right')
                if self.direction == 'n':
                    self.direction = 'e'
                elif self.direction == 'e':
                    self.direction = 's'
                elif self.direction == 's':
                    self.direction = 'w'
                elif self.direction == 'w':
                    self.direction = 'n'
        elif self.maze[y][x] == '/':
            if self.direction == 's':
                self.direction = 'w'
            elif self.direction == 'w':
                self.direction = 's'
            elif self.direction == 'n':
                self.direction = 'e'
            elif self.direction == 'e':
                self.direction = 'n'
        elif self.maze[y][x] == '\\':
            if self.direction == 's':
                self.direction = 'e'
            elif self.direction == 'e':
                self.direction = 's'
            elif self.direction == 'n':
                self.direction = 'w'
            elif self.direction == 'w':
                self.direction = 'n'
        
        dy, dx = direction.get(self.direction)
        self.X = self.X + dx
        self.Y = self.Y + dy
        
        for cart in carts:
            if cart != self:
                if cart.X == self.X and cart.Y == self.Y:
                    # collision = True
                    print(f"{move_count:>5}: Collision at X,Y: {self.X},{self.Y}")
                    collided_carts.append(cart)
                    collided_carts.append(self)


cart_id = 0
for y in range(len(track)):
    for x in range(len(track[y])):
        if track[y][x] == '>':
            carts.append(Cart(x, y, 'e', track, cart_id))
            cart_id += 1
        elif track[y][x] == 'v':
            carts.append(Cart(x, y, 's', track, cart_id))
            cart_id += 1
        elif track[y][x] == '<':
            carts.append(Cart(x, y, 'w', track, cart_id))
            cart_id += 1
        elif track[y][x] == '^':
            carts.append(Cart(x, y, 'n', track, cart_id))
            cart_id += 1

for cart in carts:
    print(f'Cart {cart.id:2} started at {cart.X:3},{cart.Y:3} and moving {cart.direction}')

move_count = 0
while len(carts) > 1:
    move_count += 1
    collided_carts = []
    # the following line the lack of) cost me ~ 3 hours of bughunting,
    # because one should read the task properly. Don't be like me,
    # don't ignore the important stuff
    carts.sort(key = attrgetter("Y", "X"))
    for cart in carts:
        cart.move()
    carts = [cart for cart in carts if cart not in collided_carts]
    if len(collided_carts) > 0:
        string = f"{move_count:>5}: "
        for cart in collided_carts:
            string += f'{cart.X},{cart.Y} - cart {cart.id:2} has collided '
        
        print(string)
        if len(carts) == 3:
            print(f'{string}Three carts moving')
        if len(carts) == 1:
            for cart in carts:
                print(f'{move_count:>5}: Last cart at {cart.X},{cart.Y} moving {cart.direction}')
