import numpy as np
from PIL import Image
from datetime import datetime

dt = datetime.now()
area = ''
with open("d18.txt") as f:
    for line in f:
        area += line.replace('.', '1 ').replace('#', '3 ').replace('|', '2 ').strip() + '; '

# . = 1 == open
# # = 3 == lumberyard
# | = 2 == tree
area = np.matrix(area[:-2])
# area = np.asarray(area)
size = len(area)
print(area)


# def lumber(X, y, x):
#     xm = -1 if x > 0 else 0
#     ym = -1 if y > 0 else 0
#     xp = 2 if x < size else 1
#     yp = 2 if y < size else 1
#     current_value = X[y, x]
#     if current_value == 0:
#         return 1 if len(np.where(X[y+ym:y+yp, x+xm:x+xp] == 1)[0]) >= 3 else current_value
#     elif current_value == 1:
#         return 2 if len(np.where(X[y+ym:y+yp, x+xm:x+xp] == 2)[0]) >= 3 else current_value
#     elif current_value == 2:
#         return current_value if len(np.where(X[y + ym:y + yp, x + xm:x + xp] == 2)[0]) >= 2\
#             and len(np.where(X[y + ym:y + yp, x + xm:x + xp] == 1)[0]) >= 1 else 0


def lumber2(X):
    Z = np.zeros((size + 2, size + 2), dtype = int)
    Z[1:-1, 1:-1] = X
    # number of open spaces
    a = np.matrix([np.count_nonzero(Z[j - 1:j + 2, i - 1:i + 2] == 1) for j in range(size + 2) for i in range(size + 2)]).reshape((size + 2, size + 2))[1:-1,
        1:-1]
    # print(a)
    # number of trees
    b = np.matrix([np.count_nonzero(Z[j - 1:j + 2, i - 1:i + 2] == 2) for j in range(size + 2) for i in range(size + 2)]).reshape((size + 2, size + 2))[1:-1,
        1:-1]
    # print(b)
    # number of lumber mills
    c = np.matrix([np.count_nonzero(Z[j - 1:j + 2, i - 1:i + 2] == 3) for j in range(size + 2) for i in range(size + 2)]).reshape((size + 2, size + 2))[1:-1,
        1:-1]
    # print(c)
    
    a_mask = np.matrix([2 if X[j, i] == 1 and b[j, i] >= 3 else
                        3 if X[j, i] == 2 and c[j, i] >= 3 else
                        3 if X[j, i] == 3 and b[j, i] >= 1 and c[j, i] >= 2 else
                        1 if X[j, i] == 3 and (b[j, i] <= 1 or c[j, i]) <= 2 else
                        X[j, i]
                        for j in range(size) for i in range(size)]).reshape((size, size))
    
    # print(a_mask)
    return a_mask


images = []


def mk_img():
    image = Image.new('RGB', (size, size), "black")
    pixels = image.load()
    for y in range(size):
        for x in range(size):
            if area[y, x] == 1:
                pixels[y, x] = 255, 255, 255
            elif area[y, x] == 2:
                pixels[y, x] = 0, 255, 0
            elif area[y, x] == 3:
                pixels[y, x] = 255, 0, 0
    
    # image.show()
    scale_factor = 8
    image = Image.Image.resize(image, (size*scale_factor, size*scale_factor))
    images.append(image)


scores = []
last_diff = 0
diff_count = 30
# for i in range(10000):
i = 0
while True:
    area = lumber2(area)
    score = len(np.where(area == 2)[0])*len(np.where(area == 3)[0])
    string = ''
    if score in scores:
        
        minute = [i for i, s in enumerate(scores) if s == score][-1]
        diff = i - minute
        if diff == last_diff:
            diff_count -= 1
        else:
            last_diff = diff
            diff_count = diff
        if diff_count <= 0:
            if (10**9 - i)%diff == 0:
                print(score)
                break
        string += f' !!! this score was last time noticed at minute{minute:>4} | diff{diff:3}'
    scores.append(score)
    print(f"{i:>4} minute:{score:>7} is the current score{string}")
    i += 1
    mk_img()
images[0].save('anim.gif', save_all = True, append_images = images[1:], duration = 1, loop = 0)

print((10**9)%27)
print((10**9 - i)%27)

print(f"Number of wood areas is {len(np.where(area == 2)[0])}")
print(f"Number of lumber yards is {len(np.where(area == 3)[0])}")
print(f"the answer is  {len(np.where(area == 2)[0])*len(np.where(area == 3)[0])}")

print(datetime.now() - dt)

# for i in range(diff):
#     mk_img()
#     area = lumber2(area)
# images[0].save('anim.gif', save_all = True, append_images = images[1:], duration = 1, loop = 0)
# with Image.open('anim.gif') as f2:
#     f2.info['duration'] = 100
#     f2.save('anim2.gif', save_all = True)
