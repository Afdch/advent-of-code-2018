# region import
from operator import itemgetter
import numpy as np
import pandas as pd
from pandas.plotting import andrews_curves


def part_2():
    
    xx = np.array([], dtype = int)
    
    prev_r1s = np.array([], dtype = int)
    r0 = 2
    r1 = 0
    r2 = 0
    r3 = 0
    r5 = 0
    r1 = 0  # ip 5
    outer_cycles = 0
    while r1 != r0:
        # outer cycle:
        
        r5 = r1 | 65536             # ip 6
        r1 = 8586263  # ip 7
        while True:
            
            r2 = r5 & 255               # ip 8
            r1 += r2                    # ip 9
            r1 &= 16777215              # ip 10
            r1 *= 65899                 # ip 11
            r1 &= 16777215              # ip 12
            r2 = 1 if r5 < 256 else 0   # ip 13
            if r2 == 0:
            # r4 (index) += r2 -> next ip 14 (r2 = 0) or ip 15 (r2 = 1)
            # ip 14 -> ip 16 -> ip 17 -> r2 = 0 -> r3 = 1 if (r2 + 1) * 256 > r5 else 0
                # r4 += r3 -> r21 or r22:
                # ip 21 -> ip 23 -> r2 += 1 -> ip 17
                # ip 22 -> ip 25 -> r5 = r2 -> r4 = 7 -> ip 7
            # ip 15 -> ip 27 -> r2 = 1 if r0 = r1 else 0 -> r4 += r2 -> ip 30 or ip 31
                # ip 30 -> ip 5
                
                
                # while 256*(r2 + 1) <= r5:
                #     r2 += 1
                # else:
                #     r5 = r2
                r2 = int(r5 / 256)
                r5 = r2
                
            else:
                break

        if r1 in prev_r1s:
            print(len([x for x in prev_r1s if x == r1]))
            if len([x for x in prev_r1s if x == r1]) >= 0:
                print(f'done at {r1}, previous value was {prev_r1s[-1]}' )
                source = pd.DataFrame(
                    {
                        'Outer Cycles': xx,
                        'Lowest non-negative integer': prev_r1s
                    }
                )
                break
            pass
        outer_cycles += 1
        xx = np.append(xx, [outer_cycles])
        prev_r1s = np.append(prev_r1s, [r1])
        print(np.min(prev_r1s), r1)
        


part_2()