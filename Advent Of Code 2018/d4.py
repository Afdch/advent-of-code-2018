import re
from operator import itemgetter
log = []
pattern = re.compile(r'\[(\d{4}-\d{2}-\d{2} \d{2}:\d{2})] (.+)')
logpattern = re.compile(r'.+#(\d+).+')

with open("d4.txt") as f:
	for line in f:
		log.append(pattern.match(line.strip()).groups())
log = sorted(log, key = itemgetter(0))

guards = [[] for i in range(10000)]
index = 0
for logitem in log:
	if logpattern.match(logitem[1]) != None:
		index = int(logpattern.match(logitem[1]).groups()[0])
	guards[index].append(logitem)
guards = [[guards.index(x), 0, x] for x in guards if x]

for guard in guards:
	#print("Guard #", guard[0])
	datet = 0
	for logitem in guard[2]:
		dt = int(logitem[0][-2:])
		if logpattern.match(logitem[1]) != None:
			continue
		elif logitem[1] == "falls asleep":
			datet -= dt
		elif logitem[1] == "wakes up":
			datet += dt
	guard[1]= datet

for guard in guards:
	print(guard[0], "slept minutes: ", guard[1])

sleepguard = sorted(guards, key=itemgetter(1))[-1]
minutes = [0]*60

def guardSleepMinutes (sleepguard):
	minutes = [0]*60
	for shiftID in range(len(sleepguard[2])):
		if logpattern.match(sleepguard[2][shiftID][1]) != None:
			continue
		elif sleepguard[2][shiftID][1] == "falls asleep":
			for i in range(int(sleepguard[2][shiftID][0][-2:]), int(sleepguard[2][shiftID+1][0][-2:])):
				minutes[i] += 1
	return minutes

def maxMinuteSleep (sleepguard):
	minutes = guardSleepMinutes(sleepguard)
	return (sleepguard[0], minutes.index(sorted(minutes)[-1]), sorted(minutes)[-1])

minutes = guardSleepMinutes(sleepguard)

print("Max minute of guard #",sleepguard[0],"is", minutes.index(sorted(minutes)[-1]))
print("the answer for part 1 is:", sleepguard[0] * maxMinuteSleep(sleepguard)[1])

allGuardSleepMinute = [ maxMinuteSleep(guard) for guard in guards]
for guard in allGuardSleepMinute:
	print (guard[0], ": ", guard[1], guard[2], guard[0]*guard[1])
guard = sorted(allGuardSleepMinute, key = itemgetter(2))[-1]
print (guard[0], "guard has has slept minute ", guard[1], guard[2], "times, the answer for part 2 is", guard[0]*guard[1])