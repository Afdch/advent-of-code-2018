import re
import time
from datetime import datetime

dt = datetime.now()
print(datetime.now() - dt)

instructions = [[0 for i in range(26)] for j in range(26)]

pattern = re.compile(r'Step ([A-Z]) must be finished before step ([A-Z]) can begin.')


def print_matrix():
    for instruction in instructions:
        print(instruction)


with open("d7.txt") as f:
    for line in f:
        prev_operation, next_operation = pattern.match(line.strip()).groups()
        instructions[ord(next_operation) - 65][ord(prev_operation) - 65] = 1

# print_matrix()
new_instructions = instructions
assemble = []
scheduled = []
pending = [x for x in range(26)]
while len(pending) > 0:
    new_inst = [x for x in new_instructions if instructions.index(x) not in assemble]
    for x in new_inst:
        print(x)
    for line in new_inst:
        if sum(line) > 0:
            continue
        index = instructions.index(line)
        scheduled.append(index)
        print(chr(index + 65), assemble)
    
    # TODO: for line in new_inst: line[ALL INDEXES IN ASSEMBLE] = 0
    for instruction in scheduled:
        for line in new_inst:
            line[instruction] = 0
        assemble.append(instruction)
    scheduled.clear()
    
    for instr in pending:
        if instr in assemble:
            pending.remove(instr)

print(datetime.now() - dt)
