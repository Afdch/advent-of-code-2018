# region import
ip_reg = 0
commands = []
with open('d19.txt') as f:
    for line in f:
        if r'#ip' in line:
            ip_reg = int(line.strip()[4:])
        else:
            command, A, B, C = line.strip().split(' ')
            A, B, C = int(A), int(B), int(C)
            commands.append((command, A, B, C))


# endregion

# region command definitions:

def addr(state_before, command):
    """addr (add register) stores into register C
    the result of adding register A and register B."""
    z, A, B, C = command
    state_before[C] = state_before[A] + state_before[B]
    return state_before


def addi(state_before, command):
    """addi (add immediate) stores into register C
    the result of adding register A and value B."""
    z, A, B, C = command
    state_before[C] = state_before[A] + B
    return state_before


def mulr(state_before, command):
    """mulr (multiply register) stores into register C
    the result of multiplying register A and register B."""
    z, A, B, C = command
    state_before[C] = state_before[A]*state_before[B]
    return state_before


def muli(state_before, command):
    """muli (multiply immediate) stores into register C
    the result of multiplying register A and value B."""
    z, A, B, C = command
    state_before[C] = state_before[A]*B
    return state_before


def banr(state_before, command):
    """banr (bitwise AND register) stores into register C
    the result of the bitwise AND of register A and register B."""
    z, A, B, C = command
    state_before[C] = state_before[A] & state_before[B]
    return state_before


def bani(state_before, command):
    """bani (bitwise AND immediate) stores into register C
    the result of the bitwise AND of register A and value B."""
    z, A, B, C = command
    state_before[C] = state_before[A] & B
    return state_before


def borr(state_before, command):
    """ borr (bitwise OR register) stores into register C
    the result of the bitwise OR of register A and register B. """
    z, A, B, C = command
    state_before[C] = state_before[A] | state_before[B]
    return state_before


def bori(state_before, command):
    """ bori (bitwise OR immediate) stores into register C
    the result of the bitwise OR of register A and value B. """
    z, A, B, C = command
    state_before[C] = state_before[A] | B
    return state_before


def setr(state_before, command):
    """ setr (set register) copies the contents of register A into register C.
    (Input B is ignored.) """
    z, A, B, C = command
    state_before[C] = state_before[A]
    return state_before


def seti(state_before, command):
    """ seti (set immediate) stores value A into register C.
    (Input B is ignored.) """
    z, A, B, C = command
    state_before[C] = A
    return state_before


def gtir(state_before, command):
    """ gtir (greater-than immediate/register) sets register C to 1
    if value A is greater than register B. Otherwise, register C is set to 0. """
    z, A, B, C = command
    state_before[C] = 1 if A > state_before[B] else 0
    return state_before


def gtri(state_before, command):
    """ gtri (greater-than register/immediate) sets register C to 1
    if register A is greater than value B. Otherwise, register C is set to 0. """
    z, A, B, C = command
    state_before[C] = 1 if state_before[A] > B else 0
    return state_before


def gtrr(state_before, command):
    """ gtrr (greater-than register/register) sets register C to 1
    if register A is greater than register B. Otherwise, register C is set to 0. """
    z, A, B, C = command
    state_before[C] = 1 if state_before[A] > state_before[B] else 0
    return state_before


def eqir(state_before, command):
    """ eqir (equal immediate/register) sets register C to 1
    if value A is equal to register B. Otherwise, register C is set to 0. """
    z, A, B, C = command
    state_before[C] = 1 if A == state_before[B] else 0
    return state_before


def eqri(state_before, command):
    """ eqri (equal register/immediate) sets register C to 1
    if register A is equal to value B. Otherwise, register C is set to 0. """
    z, A, B, C = command
    state_before[C] = 1 if state_before[A] == B else 0
    return state_before


def eqrr(state_before, command):
    """ eqrr (equal register/register) sets register C to 1
    if register A is equal to register B. Otherwise, register C is set to 0. """
    z, A, B, C = command
    state_before[C] = 1 if state_before[A] == state_before[B] else 0
    return state_before


def switch_comm(state, command):
    return {
        'muli': lambda state, command: muli(state, command),
        'seti': lambda state, command: seti(state, command),
        'bani': lambda state, command: bani(state, command),
        'gtri': lambda state, command: gtri(state, command),
        'gtrr': lambda state, command: gtrr(state, command),
        'eqrr': lambda state, command: eqrr(state, command),
        'addi': lambda state, command: addi(state, command),
        'gtir': lambda state, command: gtir(state, command),
        'eqir': lambda state, command: eqir(state, command),
        'mulr': lambda state, command: mulr(state, command),
        'addr': lambda state, command: addr(state, command),
        'borr': lambda state, command: borr(state, command),
        'bori': lambda state, command: bori(state, command),
        'eqri': lambda state, command: eqri(state, command),
        'banr': lambda state, command: banr(state, command),
        'setr': lambda state, command: setr(state, command),
    }.get(command[0])(state, command)


# endregion


# region part 1:
def part1():
    ip = 0
    ip_max = len(commands)
    # part 1
    state = [0, 0, 0, 0, 0, 0]
    # part 2
    state = [1, 0, 0, 0, 0, 0]
    print(f'initial state \t\t-> {state}')
    while 0 <= ip < ip_max:
        operation = commands[ip]
        state[ip_reg] = ip
        
        state = switch_comm(state, operation)
        ip = state[ip_reg]
        ip += 1
        print(f"{operation}\t-> {state}")


# endregion

def part2(number):
    sum_divisors = 0
    for i in range(1, number + 1):
        if number%i == 0:
            sum_divisors += i
    return sum_divisors


print(part2(967))
print(part2(10551367))
