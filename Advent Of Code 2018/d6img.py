import re
from PIL import Image
import randomcolor

rclr = randomcolor.RandomColor()
coordinates = []
pattern = re.compile(r'(\d+), (\d+)')

with open("d6.txt") as f:
	for line in f:
		coordinates.append((int(pattern.match(line.strip()).group(1)), int(pattern.match(line.strip()).group(2))))

areaSizeLen = 370
area = [[0 for j in range(areaSizeLen)] for i in range(areaSizeLen)]
for lineInd in range(areaSizeLen):
	for squareInd in range(areaSizeLen):
		distances = []
		# coordinates = [(1,1), (1, 9), (9,5)]
		for coordinate in coordinates:
			distances.append(
				(abs(coordinate[0] - squareInd) + abs(coordinate[1] - lineInd), coordinates.index(coordinate)))
		distances.sort()
		if distances[0][0] == distances[1][0]:
			area[lineInd][squareInd] = ''
		else:
			area[lineInd][squareInd] = distances[0][1]
"""
palette = [	(159, 168, 218),	(179, 157, 219),	(206, 147, 216),	(244, 143, 177),
			(239, 154, 154),	(120, 144, 156),	(189, 189, 189),	(141, 110, 99),
			(255, 112, 67),		(255, 167, 38),		(255, 202, 40),		(255, 238, 88),
			(212, 225, 87),		(156, 204, 101),	(102, 187, 106),	(38, 166, 154),
			(38, 198, 218),		(41, 182, 246),		(66, 165, 245),		(92, 107, 192),
			(126, 87, 194),		(171, 71, 188),		(236, 64, 122),		(239, 83, 80),
			(55, 71, 79),		(66, 66, 66),		(78, 52, 46),		(216, 67, 21),
			(239, 108, 0),		(255, 143, 0),		(249, 168, 37),		(158, 157, 36),
			(85, 139, 47),		(46, 125, 50),		(0, 105, 92),		(0, 131, 143),
			(2, 119, 189),		(21, 101, 192),		(40, 53, 147),		(69, 39, 160),
			(106, 27, 154),		(173, 20, 87),		(198, 40, 40),		(213, 0, 0),
			(197, 17, 98),		(170, 0, 255),		(98, 0, 234),		(48, 79, 254),
			(41, 98, 255),		(0, 145, 234),		(0, 184, 212),		(0, 191, 165),
			(0, 200, 83),		(100, 221, 23),		(174, 234, 0),		(255, 214, 0),
			(255, 171, 0),		(255, 109, 0),		(221, 44, 0), 		(33, 33, 33) ]
"""
palette = [(int(x[1:3], 16), int(x[3:5], 16), int(x[5:7], 16)) for x in rclr.generate(count = len(coordinates))]

image = Image.new('RGB', (areaSizeLen, areaSizeLen), "black")
pixels = image.load()
for i in range(image.size[0]):
	for j in range(image.size[1]):
		if isinstance(area[i][j], int):
			pixels[i, j] = palette[area[i][j]]

image.show()
