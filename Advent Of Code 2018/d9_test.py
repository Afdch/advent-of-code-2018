import unittest
import Nine


class TestD9(unittest.TestCase):
    
    def test_game1(self):
        result = Nine.game3(10, 1618)
        self.assertEqual(result, 8317)
    
    def test_game2(self):
        result = Nine.game3(13, 7999)
        self.assertEqual(result, 146373)
    
    def test_game3(self):
        result = Nine.game3(17, 1104)
        self.assertEqual(result, 2764)
    
    def test_game4(self):
        result = Nine.game3(21, 6111)
        self.assertEqual(result, 54718)
    
    def test_game5(self):
        result = Nine.game3(30, 5807)
        self.assertEqual(result, 37305)
