import numpy as np
from PIL import Image

instructions = ''

with open('d20.txt') as f:
    for line in f:
        instructions += line.strip()
# instructions = '^WSSEESWWWNW(S|NENNEEEENN(ESSSSW(NWSW|SSEN)|WSWWN(E|WWS(E|SS))))$'

mapl = np.zeros((3, 3), dtype = int)
start = 1, 1
position = 0, 0
mapl[start] = 2
directions = {'N': (-2, 0), 'S': (2, 0), 'W': (0, -2), 'E': (0, 2)}


def traverse_map(direction):
    global mapl, position, start
    dy, dx = directions[direction]
    _y, _x = mapl.shape
    pos_y = start[0] + position[0]
    pos_x = start[1] + position[1]
    if 0 > pos_y + dy or \
            pos_y + dy >= _y or \
            0 > pos_x or \
            pos_x + dx >= _x:
        new_shape = (_y + 4, _x + 4)
        new_map = np.zeros(new_shape, dtype = int)
        new_map[2: -2, 2: -2] = mapl
        
        mapl = new_map
        start = (start[0] + 2, start[1] + 2)
    position = (position[0] + dy, position[1] + dx)
    
    # 2 = floor, 1 = wall, 3 = door
    mapl[start[0] + position[0], start[1] + position[1]] = 2
    door_pos = start[0] + position[0] - int(dy/2), start[1] + position[1] - int(dx/2)
    mapl[door_pos] = 3
    wy, wx = (1, 0) if direction not in ('N', 'S') else (0, 1)
    posA = door_pos[0] - wy, door_pos[1] - wx
    posB = door_pos[0] + wy, door_pos[1] + wx
    mapl[posA] = 1
    mapl[posB] = 1
    # mk_img()


max_dist = 0
distance = 1
distances = dict({(0, 0): 0})


def parse_map(index = 0, internal = False):
    global mapl, position, instructions, max_dist, distance
    _bkp_pos = position
    _bkp_dist = distance
    while instructions[index] != (r'$' if not internal else r')'):
        if instructions[index] in directions.keys():
            
            traverse_map(instructions[index])
            print(position, distance)
            if position in distances.keys():
                if distances[position] > distance:
                    distances[position] = distance
            else:
                distances[position] = distance
            distance += 1
            if distance > max_dist:
                max_dist = distance
        
        
        elif instructions[index] == r'|':
            position = _bkp_pos
            distance = _bkp_dist
        elif instructions[index] == r'(':
            index += 1
            index, position = parse_map(index, internal = True)
        index += 1
    else:
        if internal:
            return index, _bkp_pos


def mk_img(mode = 'gif'):
    size = mapl.transpose().shape
    wall_color = 0, 0, 0
    door_color = 255, 0, 0
    goblin_color = 0, 0, 0
    floor_color = 255, 255, 255
    image = Image.new('RGB', size, goblin_color)
    pixels = image.load()
    for y in range(size[1]):
        for x in range(size[0]):
            if mapl[y, x] == 1:
                pixels[x, y] = wall_color
            elif mapl[y, x] == 2:
                pixels[x, y] = floor_color
            elif mapl[y, x] == 3:
                pixels[x, y] = door_color
    
    scale_factor = 8
    image = Image.Image.resize(image, (size[0]*scale_factor, size[1]*scale_factor))
    image.show()
    
    if mode == 'show':
        image.show()
    elif mode == 'save':
        image.save('d15.png')


parse_map()
print(max([distance for location, distance in distances.items()]))
print(len([1 for location, distance in distances.items() if distance >= 1000]))
print(distances)
mk_img()
