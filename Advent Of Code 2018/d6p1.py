import re
from operator import itemgetter
from datetime import datetime

dt = datetime.now()
print(datetime.now() - dt)

coordinates = []
pattern = re.compile(r'(\d+), (\d+)')

with open("d6.txt") as f:
	for line in f:
		coordinates.append((int(pattern.match(line.strip()).group(1)), int(pattern.match(line.strip()).group(2))))
# coordinates = sorted(coordinates, key=itemgetter(0))

areaSizeLen = 350
area = [[0 for j in range(areaSizeLen)] for i in range(areaSizeLen)]

"""
# this code takes 10+ seconds
for (x, y) in coordinates:
	print(datetime.now() - dt)
	for lineInd in range(areaSizeLen):
		distances = []
		for squareInd in range(areaSizeLen):
			distances.append((abs(x - squareInd) + abs(y - lineInd), coordinates.index((x, y))))
		dist = min(distances)
		area[lineInd][squareInd] = dist if distances.count(dist) == 1 else ''

print(datetime.now() - dt)
"""


# the following code takes about 3 seconds


def calculate_manhattan(point, sources):
    distances1 = []
    i, j = point
    for zz in range(0, len(sources)):
        yy, xx = sources[zz]
        distance = abs(i - xx) + abs(j - yy)
        distances1.append(distance)
    to_return = distances1.index(min(distances1)) + 1 if distances1.count(min(distances1)) == 1 else ''
    return to_return


for a in range(areaSizeLen):
    for b in range(areaSizeLen):
        area[a][b] = calculate_manhattan((a, b), coordinates)

print(datetime.now() - dt)
# determine border numbers -- aka infinite areas:
newArea = [y[0::areaSizeLen - 1] for y in [x for x in area]] + [x for x in area[0::len(area) - 1]]
# print (newArea)
infinites = []
for element in newArea:
	for subelement in element:
		if isinstance(subelement, int):
			infinites.append(subelement)
print(set(infinites))

areaS = [x for x in range(len(coordinates)) if x not in set(infinites)]
print(areaS)

print(datetime.now() - dt)
areasCount = []
for areas in areaS:
	count = 0
	for line in area:
		count += line.count(areas)
	z = (areas, count)
	# print(z)
	areasCount.append(z)
# part 1 answer
print(sorted(areasCount, key = itemgetter(1))[-1])

print(datetime.now() - dt)
