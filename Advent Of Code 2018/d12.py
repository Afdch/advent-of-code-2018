from collections import defaultdict
import re

rules = defaultdict()
pots = defaultdict()
initial_state = '#..######..#....#####..###.##..#######.####...####.##..#....#.##.....########.#...#.####........#.#.'

with open("d12.txt") as f:
    for line in f:
        rule = (line.strip().split(' => '))
        rules.update({rule[0]: rule[1]})

print(rules)

for char in range(len(initial_state)):
    pots.update({char: initial_state[char]})
print(pots)
print(min(pots.keys()))

for generation in range(1):
    state = pots.copy()
    for pot in range(- 2, max(state.keys()) + 2):
        key = ''
        for shift in range(-2, 3):
            key += pots.get(pot + shift) or '.'
        state.update({pot: rules.get(key)})
    pots.update(state)
    print(generation, sum([x for x in state.keys() if state.get(x) is "#"]), state)

count = 0
for pot in pots:
    if pots.get(pot) == '#':
        count += pot
print(count)
print(pots)
