cave_system_depth = 11817

target = (9, 751)


erosion_data = {}
geo_data = {}

def geo_index(x, y) -> int:
    if (x, y) not in geo_data.keys():
        if (x, y) == (0, 0):
            return 0
        elif (x, y) == target:
            return 0
        elif x == 0:
            return y*48271
        elif y == 0:
            return x*16807
        else:
            geo_data[(x, y)] = erosion(x-1, y) * erosion(x, y-1)
    return geo_data[(x, y)]


def erosion(x, y) -> int:
    if (x, y) not in erosion_data.keys():
        erosion_data[(x, y)] = (geo_index(x, y) + cave_system_depth ) % 20183
    return erosion_data[(x, y)]

def region_type(x, y) -> int:
    return erosion(x, y) % 3

def calc_risk():
    x, y = target
    risk = 0
    for dx in range(x+1):
        for dy in range(y+1):
            risk += region_type(dx, dy)
    print(risk)

# part 1
# calc_risk()

# part 2
tx, ty = target
def calc_priority(x, y):
    return abs(tx - x) + abs(ty - y)

# 0 -> rocky    |   c   t   |   neither = 0
# 1 -> wet      |   c   n   |   torch = 1
# 2 -> narrow   |   t   n   |   climbing gear = 2

def neighbors(curr):
    coord, equipment = curr
    x, y = coord
    neigh = [(X, Y) for X, Y in [(x+1, y), (x-1, y), (x, y+1), (x, y-1)]
            if X >= 0 and Y >=0 and region_type(X, Y) != equipment]
    return neigh

from queue import PriorityQueue
frontier = PriorityQueue()
# torch = 1 't', climbing gear = 2 'c', neither = 0 'n'
cost = {((0, 0), 1): 0}
frontier.put((calc_priority(0, 0), ((0, 0), 1)))
distance = None


while not frontier.empty():
    curr_priority, curr = frontier.get()
    coord, equipment = curr
    
    if distance is not None and cost[curr] > distance:
        continue
    if coord == target:
        distance = cost[curr]
    nei = neighbors(curr)
    for n_x, n_y in nei:
        n_cost = cost[curr] + 1
        n_priority = calc_priority(n_x, n_y) + n_cost
        if (next := ((n_x, n_y), equipment)) not in cost or cost[next] > n_cost:
            frontier.put((n_priority, next))
            cost[next] = n_cost
    # change equipment
    n_x, n_y = coord
    new_equipment = [a for a in [0, 1, 2] 
                    if a != equipment and 
                    a != region_type(n_x, n_y)][0]
    
    if (next := (coord, new_equipment)) not in cost or cost[next] > cost[curr] + 7:
        cost[next] = cost[curr] + 7
        frontier.put((curr_priority + 7, next))

if (t:=(target, 1)) in cost.keys():
    print(cost[t])
if (t:=(target, 2)) in cost.keys():
    print(cost[t])