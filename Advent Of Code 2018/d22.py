import numpy as np

# depth = 510
# target = 10, 10
depth = 11817
target = 751, 9
size = target[0] + 20, target[1] + 20


def geo_index(y, x):
    if y == 0 and x == 0:
        return 0
    elif y == 0:
        return x * 16807
    elif x == 0:
        return y * 48271
    else:
        return None


r_type = np.zeros(size, dtype = int)
r_er = np.zeros(size, dtype = int)
r_geo = np.zeros(size, dtype = int)
for y in range(size[0]):
    for x in range(size[1]):
        geo_ind = geo_index(y, x)
        if geo_ind == None:
            geo_ind = r_er[y-1, x] * r_er[y, x-1]
        r_geo[y, x] = geo_ind if (y, x) != target else 0
        r_er[y, x] = (r_geo[y, x] + depth) % 20183
        r_type[y, x] = r_er[y, x] % 3
# part 1
print(np.sum(np.sum(r_type[:target[0] + 1, :target[1] + 1])))


def neighbours(pos):
    y, x = pos
    neigh = []
    if y > 0:
        neigh.append((y-1, x))
    if y < size[0]:
        neigh.append((y+1, x))
    if x > 0:
        neigh.append((y, x-1))
    if x < size[1]:
        neigh.append((y, x+1))
    return neigh



def find_path(t_y, t_x):
    """frontier should keep track of current item equipped
    and locations,
    cost should keep track of all previous cost and ?item?
    should cam from keep track of item as well?"""
    
    # frontier contains tuples of (cost, position, item)
    # where item = 0 for None, 1 for torch, 2 for climbing gear
    frontier = [(0, (0, 0), 1)]
    came_from = {(0, 0): None}
    cost_so_far = {(0, 0): 0}
    lowest_cost = 7000
    
    while len(frontier) > 0:
        """"""
        frontier = [z for z in frontier if z[0] < lowest_cost]\
            .sort(key = lambda z: z[0] + abs(z[1][0] - t_y) + abs(z[1][1] - t_x))
        current_cost, current_pos = frontier.pop()
        
        """do I need early exit?"""
        # if current == (t_y, t_x):
        #     break
        
        for next in neighbours(current_pos):
            """we should add all both possible variations to the frontier pool:
            keep_item + item 1
            keep_item + item 2
            item 1 + item 2
            depending on combination of current -> next
            """
            if r_type[current_pos] == r_type[next]:
                added_cost = 1
            elif r_type[current_pos] == 0:
                added_cost = 8
                pass
            
            new_cost = current_cost + added_cost
