def addr(state_before, command):
    """addr (add register) stores into register C
    the result of adding register A and register B."""
    z, A, B, C = command
    state_before[C] = state_before[A] + state_before[B]
    return state_before


def addi(state_before, command):
    """addi (add immediate) stores into register C
    the result of adding register A and value B."""
    z, A, B, C = command
    state_before[C] = state_before[A] + B
    return state_before


def mulr(state_before, command):
    """mulr (multiply register) stores into register C
    the result of multiplying register A and register B."""
    z, A, B, C = command
    state_before[C] = state_before[A]*state_before[B]
    return state_before


def muli(state_before, command):
    """muli (multiply immediate) stores into register C
    the result of multiplying register A and value B."""
    z, A, B, C = command
    state_before[C] = state_before[A]*B
    return state_before


def banr(state_before, command):
    """banr (bitwise AND register) stores into register C
    the result of the bitwise AND of register A and register B."""
    z, A, B, C = command
    state_before[C] = state_before[A] & state_before[B]
    return state_before


def bani(state_before, command):
    """bani (bitwise AND immediate) stores into register C
    the result of the bitwise AND of register A and value B."""
    z, A, B, C = command
    state_before[C] = state_before[A] & B
    return state_before


def borr(state_before, command):
    """ borr (bitwise OR register) stores into register C
    the result of the bitwise OR of register A and register B. """
    z, A, B, C = command
    state_before[C] = state_before[A] | state_before[B]
    return state_before


def bori(state_before, command):
    """ bori (bitwise OR immediate) stores into register C
    the result of the bitwise OR of register A and value B. """
    z, A, B, C = command
    state_before[C] = state_before[A] | B
    return state_before


def setr(state_before, command):
    """ setr (set register) copies the contents of register A into register C.
    (Input B is ignored.) """
    z, A, B, C = command
    state_before[C] = state_before[A]
    return state_before


def seti(state_before, command):
    """ seti (set immediate) stores value A into register C.
    (Input B is ignored.) """
    z, A, B, C = command
    state_before[C] = A
    return state_before


def gtir(state_before, command):
    """ gtir (greater-than immediate/register) sets register C to 1
    if value A is greater than register B. Otherwise, register C is set to 0. """
    z, A, B, C = command
    state_before[C] = 1 if A > state_before[B] else 0
    return state_before


def gtri(state_before, command):
    """ gtri (greater-than register/immediate) sets register C to 1
    if register A is greater than value B. Otherwise, register C is set to 0. """
    z, A, B, C = command
    state_before[C] = 1 if state_before[A] > B else 0
    return state_before


def gtrr(state_before, command):
    """ gtrr (greater-than register/register) sets register C to 1
    if register A is greater than register B. Otherwise, register C is set to 0. """
    z, A, B, C = command
    state_before[C] = 1 if state_before[A] > state_before[B] else 0
    return state_before


def eqir(state_before, command):
    """ eqir (equal immediate/register) sets register C to 1
    if value A is equal to register B. Otherwise, register C is set to 0. """
    z, A, B, C = command
    state_before[C] = 1 if A == state_before[B] else 0
    return state_before


def eqri(state_before, command):
    """ eqri (equal register/immediate) sets register C to 1
    if register A is equal to value B. Otherwise, register C is set to 0. """
    z, A, B, C = command
    state_before[C] = 1 if state_before[A] == B else 0
    return state_before


def eqrr(state_before, command):
    """ eqrr (equal register/register) sets register C to 1
    if register A is equal to register B. Otherwise, register C is set to 0. """
    z, A, B, C = command
    state_before[C] = 1 if state_before[A] == state_before[B] else 0
    return state_before


def part1():
    with open('d16.txt') as f:
        i = 0
        state = ''
        mul_op_number = 0
        uniq_commands = ''
        # manual detections because I am lazy
        for line in f:
            if line.strip():
                state += line
            else:
                try:
                    multiple_opcodes = 0
                    string = ''
                    a, b, c = state.strip().split('\n')
                    a = [int(z) for z in a[9:-1].split(', ')]
                    b = [int(z) for z in b.split(' ')]
                    c = [int(z) for z in c[9:-1].split(', ')]
                    print(i, a, b, c)
                    
                    if addi(a.copy(), b) == c:
                        string += f'Command {b[0]} can be addi command\n'
                        multiple_opcodes += 1
                    if addr(a.copy(), b) == c:
                        string += f'Command {b[0]} can be addr command\n'
                        multiple_opcodes += 1
                    
                    if mulr(a.copy(), b) == c:
                        string += f'Command {b[0]} can be mulr command\n'
                        multiple_opcodes += 1
                    if muli(a.copy(), b) == c:
                        string += f'Command {b[0]} can be muli command\n'
                        multiple_opcodes += 1
                    
                    if bani(a.copy(), b) == c:
                        string += f'Command {b[0]} can be bani command\n'
                        multiple_opcodes += 1
                    if banr(a.copy(), b) == c:
                        string += f'Command {b[0]} can be banr command\n'
                        multiple_opcodes += 1
                    
                    if bori(a.copy(), b) == c:
                        string += f'Command {b[0]} can be bori command\n'
                        multiple_opcodes += 1
                    if borr(a.copy(), b) == c:
                        string += f'Command {b[0]} can be borr command\n'
                        multiple_opcodes += 1
                    
                    if seti(a.copy(), b) == c:
                        string += f'Command {b[0]} can be setr command\n'
                        multiple_opcodes += 1
                    if setr(a.copy(), b) == c:
                        string += f'Command {b[0]} can be setr command\n'
                        multiple_opcodes += 1
                    
                    if gtir(a.copy(), b) == c:
                        string += f'Command {b[0]} can be gtir command\n'
                        multiple_opcodes += 1
                    if gtri(a.copy(), b) == c:
                        string += f'Command {b[0]} can be gtri command\n'
                        multiple_opcodes += 1
                    if gtrr(a.copy(), b) == c:
                        string += f'Command {b[0]} can be gtrr command\n'
                        multiple_opcodes += 1
                    
                    if eqir(a.copy(), b) == c:
                        string += f'Command {b[0]} can be eqir command\n'
                        multiple_opcodes += 1
                    if eqri(a.copy(), b) == c:
                        string += f'Command {b[0]} can be eqri command\n'
                        multiple_opcodes += 1
                    if eqrr(a.copy(), b) == c:
                        string += f'Command {b[0]} can be eqrr command\n'
                        multiple_opcodes += 1
                    
                    if multiple_opcodes >= 3:
                        mul_op_number += 1
                    elif multiple_opcodes == 1:
                        uniq_commands += string
                    
                    i += 1
                    state = ''
                except ValueError:
                    continue
        
        print(f'{mul_op_number} commands can be interpreted in 3 or more ways')
        print(uniq_commands)


def part2_preparations():
    with open('d16.txt') as f:
        i = 0
        state = ''
        mul_op_number = 0
        uniq_commands = ''
        # manual detections because I am lazy
        known_commands = [3, 4, 8, 5, 13, 7, 14, 15, 2, 12, 1, 11, 9, 6, 10]
        for line in f:
            if line.strip():
                state += line
            else:
                
                multiple_opcodes = 0
                string = ''
                a, b, c = state.strip().split('\n')
                a = [int(z) for z in a[9:-1].split(', ')]
                b = [int(z) for z in b.split(' ')]
                c = [int(z) for z in c[9:-1].split(', ')]
                print(i, a, b, c)
                if b[0] in known_commands:
                    i += 1
                    state = ''
                    continue
                # addi == 6
                # if addi(a.copy(), b) == c:
                #     string += f'Command {b[0]} can be addi command\n'
                #     multiple_opcodes += 1
                # addr == 10
                # if addr(a.copy(), b) == c:
                #     string += f'Command {b[0]} can be addr command\n'
                #     multiple_opcodes += 1
                
                # mulr == 9
                # if mulr(a.copy(), b) == c:
                #     string += f'Command {b[0]} can be mulr command\n'
                #     multiple_opcodes += 1
                # muli == 0
                # if muli(a.copy(), b) == c:
                #     string += f'Command {b[0]} can be muli command\n'
                #     multiple_opcodes += 1
                
                # bani - 2
                # if bani(a.copy(), b) == c:
                #     string += f'Command {b[0]} can be bani command\n'
                #     multiple_opcodes += 1
                # banr - 14
                # if banr(a.copy(), b) == c:
                #     string += f'Command {b[0]} can be banr command\n'
                #     multiple_opcodes += 1
                
                # command 12
                # if bori(a.copy(), b) == c:
                #     string += f'Command {b[0]} can be bori command\n'
                #     multiple_opcodes += 1
                # if borr(a.copy(), b) == c:
                #     string += f'Command {b[0]} can be borr command\n'
                #     multiple_opcodes += 1
                
                # command 1 == seti
                # if seti(a.copy(), b) == c:
                #     string += f'Command {b[0]} can be seti command\n'
                #     multiple_opcodes += 1
                # Command 15 == setr
                # if setr(a.copy(), b) == c:
                #     string += f'Command {b[0]} can be setr command\n'
                #     multiple_opcodes += 1
                
                # command 7 == gtir
                # if gtir(a.copy(), b) == c:
                #     string += f'Command {b[0]} can be gtir command\n'
                #     multiple_opcodes += 1
                # Command 3 == gtri
                # if gtri(a.copy(), b) == c:
                #    string += f'Command {b[0]} can be gtri command\n'
                #    multiple_opcodes += 1
                # Command 4 == gtrr
                # if gtrr(a.copy(), b) == c:
                #     string += f'Command {b[0]} can be gtrr command\n'
                #     multiple_opcodes += 1
                
                # command 8 == eqir
                # if eqir(a.copy(), b) == c:
                #     string += f'Command {b[0]} can be eqir command\n'
                #     multiple_opcodes += 1
                # command 5 == eqrr
                # if eqri(a.copy(), b) == c:
                #     string += f'Command {b[0]} can be eqri command\n'
                #     multiple_opcodes += 1
                # command 13 == eqri
                # if eqrr(a.copy(), b) == c:
                #     string += f'Command {b[0]} can be eqrr command\n'
                #     multiple_opcodes += 1
                
                if multiple_opcodes >= 3:
                    mul_op_number += 1
                
                elif multiple_opcodes == 1:
                    uniq_commands += string
                
                print(string)
                i += 1
                state = ''
        
        print(f'{mul_op_number} commands can be interpreted in 3 or more ways')
        print(uniq_commands)


def switch_comm(state, command):
    return {
        0: lambda state, command: muli(state, command),
        1: lambda state, command: seti(state, command),
        2: lambda state, command: bani(state, command),
        3: lambda state, command: gtri(state, command),
        4: lambda state, command: gtrr(state, command),
        5: lambda state, command: eqrr(state, command),
        6: lambda state, command: addi(state, command),
        7: lambda state, command: gtir(state, command),
        8: lambda state, command: eqir(state, command),
        9: lambda state, command: mulr(state, command),
        10: lambda state, command: addr(state, command),
        11: lambda state, command: borr(state, command),
        12: lambda state, command: bori(state, command),
        13: lambda state, command: eqri(state, command),
        14: lambda state, command: banr(state, command),
        15: lambda state, command: setr(state, command),
    }.get(command[0])(state, command)


def part2():
    state = [0, 0, 0, 0]
    print(f"\t\t\t\t {state}")
    with open('d16-2.txt') as f:
        for line in f:
            operation = [int(z) for z in line.strip().split(' ')]
            state = switch_comm(state, operation)
            print(f"{operation}, \t {state}")


part2()
