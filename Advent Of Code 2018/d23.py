import re
from collections import defaultdict
from operator import itemgetter
import z3


pattern = re.compile(r'pos=<(.+)>, r=(.+)')

bots = []
with open("d23_test.txt") as f:
    for line in f:
        pos, r = pattern.match(line.strip()).groups()
        pos = tuple(map(int, pos.split(',')))
        r = int(r)
        bots.append((pos, r))


# part 1
# bots.sort(key = itemgetter(1))
# bot_range = bots[-1]
# print(len(bots))
# print(bot_range)
# bots_in_range = [bot for bot in bots if
#                  abs(bot[0][0] - bot_range[0][0]) +
#                  abs(bot[0][1] - bot_range[0][1]) +
#                  abs(bot[0][2] - bot_range[0][2]) <=
#                  bot_range[1]]
# print(len(bots_in_range))

# part 2
min_x = sorted(bots, key = lambda xx: xx[0][0] - xx[1])[0][0][0]
max_x = sorted(bots, key = lambda xx: xx[0][0] + xx[1])[-1][0][0]
min_y = sorted(bots, key = lambda xx: xx[0][1] - xx[1])[0][0][1]
max_y = sorted(bots, key = lambda xx: xx[0][1] + xx[1])[-1][0][1]
min_z = sorted(bots, key = lambda xx: xx[0][2] - xx[1])[0][0][2]
max_z = sorted(bots, key = lambda xx: xx[0][2] + xx[1])[-1][0][2]

# bots = [((x + abs(min_x), y + abs(min_y), z + abs(min_z)), radius) for ((x, y, z), radius) in bots]

#
print(min_z, min_y, min_x)

# bots_r = {(x, y, z): 0
#           for x in range(min_x, max_x + 1)
#           for y in range(min_y, max_y + 1)
#           for z in range(min_z, max_z + 1)}#
# bots_r = {}#
#
# print('library')
# for z in range(min_z, max_z + 1):
#     for y in range(min_y, max_y + 1):
#         for x in range(min_x, max_x + 1):
#             bots_r[x, y, z] = 0
#             for bot in bots:
#                 if abs(bot[0][0] - x) +\
#                    abs(bot[0][1] - y) +\
#                    abs(bot[0][2] - z) <=\
#                    bot[1]:
#                     bots_r[(x, y, z)] += 1
#             print(x)
#         print(y)
#     print(z)
#
# print('all')
#
# zzz = sorted([key for key in bots_r.keys() if bots_r[key] is max(bots_r.values())], key = lambda x: x[0] + x[1] + x[2])
# print(zzz)



# bots_r = defaultdict(int)
# for (x0, y0, z0), r0 in bots:
#     for x in range(x0 - r0, x0 + r0 + 1):
#         for y in range(y0 - r0, y0 + r0 + 1):
#             for z in range(z0 - r0, z0 + r0 + 1):
#                 if abs(x-x0) + abs(y-y0) + abs(z-z0) <= r0:
#                     bots_r[(x, y, z)] += 1
#                 # print('z done')
#             print('y done')
#         print('x done')
#     print('bot done')
# xs, ys, zs = 0, 0, 0
# distx, disty, distz = (max_x + abs(min_x)) // 2, (max_y + abs(min_y)) // 2, (max_z + abs(min_z)) // 2
# xs = [min_x, max_x]
# ys = [min_y, max_y]
# zs = [min_z, max_z]
# while abs(distx) > 0 or abs(disty) > 0 or abs(distz) > 0:
#     drs = []
#     for x in range(min(xs) + distx//2, max(xs) + distx//2 + 1, distx + 1):
#         for y in range(min(ys) + disty//2, max(ys) + disty//2 + 1, disty + 1):
#             for z in range(min(zs) + distz//2, max(zs) + distz//2 + 1, distz + 1):
#                 drs.append(((x, y, z), len([1 for (x1, y1, z1), r1 in bots if abs(x1-x) + abs(y1-y) + abs(z1-z) <= r1])))
#     # print(drs)
#     drs.sort(key = lambda xz: xz[1], reverse = True)
#     best = drs[0][0]
#     distx //= 2
#     disty //= 2
#     distz //= 2
#     xs = [best[0] - distx, best[0] + distx]
#     ys = [best[1] - disty, best[1] + disty]
#     zs = [best[2] - distz, best[2] + distz]
#
#     print(xs, ys, zs, len(drs))
#
# # drs = [((x - abs(min_x), y - abs(min_y), z - abs(min_z)), radius) for ((x, y, z), radius) in drs]
# # drs.sort(key = lambda x: abs(x[0][0] - min_x) + abs(x[0][1] - min_y) + abs(x[0][2] - min_z))
# drs.sort(key = lambda xzt: xzt[1], reverse = True)
# print(drs[0])
# print(drs)
# print(abs(drs[0][0][0]) + abs(drs[0][0][1]) + abs(drs[0][0][2]))


# 109263433 is too high


sum_w = 0
sum_x = 0
sum_y = 0
sum_z = 0
for (xi, yi, zi), ri in bots:
    w = sum([1 for ((x, y, z), r) in bots if abs(x-xi) + abs(y-yi) + abs(z-zi) <= ri])
    sum_x += w * xi
    sum_y += w * yi
    sum_z += w * zi
    sum_w += w
X = sum_x//sum_w
Y = sum_y//sum_w
Z = sum_z//sum_w
R = sorted(bots, key = lambda r: -r[1])[0][1]
W = sum([1 for ((x, y, z), r) in bots if abs(x-X) + abs(y-Y) + abs(z-Z) <= r])
print(X, Y, Z, R)
print(W)

selected = 0
rds = 10
for zq in range(Z - rds, Z + rds):
    for yq in range(Y - rds, Y + rds):
        for xq in range(X - rds, X + rds):
            w = sum([1 for ((x, y, z), r) in bots if abs(x-xq) + abs(y-yq) + abs(z-zq) <= r])
            if w > W:
                W = w
                selected = 0
            if w == W and (selected == 0 or abs(xq) + abs(yq) + abs(zq) < abs(selected[0]) + abs(selected[1]) + abs(selected[2])):
                selected = (xq, yq, zq)
        print((yq, zq), W, selected)
    print(zq, W, selected)
print(W, selected)
    
