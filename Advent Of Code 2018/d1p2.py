currFreq = 0
freqs = []
freqsChanges = []
with open("d1.txt") as f:
	for line in f:
		freqsChanges.append(int(line.strip()))
i = 0
loopsCount = 0
while (freqs.count(currFreq) < 2):
	if (i >= len(freqsChanges)):
		loopsCount += 1
		print("Loops count: ",  loopsCount)
		i = 0
	currFreq += freqsChanges[i]
	freqs.append(currFreq)
	i += 1

print(currFreq)
print(freqs.count(currFreq))