from operator import attrgetter, itemgetter


def heuristic(a, b): return abs(a[0] - b[0]) + abs(a[1] - b[1])


class Graph:
    def __init__(self):
        self.grid = {}
    
    def add(self, position, value):
        """value: 0 for cavern,  1 for walls,
                  2 for Goblins, 3 for elves    """
        self.grid[position] = value
    
    def neighbours(self, position):
        neigh = []
        x, y = position
        condition = self.grid[position] != 1  # if unit_search else self.grid[position] == 0
        if condition:
            # just filters out the walls, other obstacles filtered in find_path
            if self.grid[(x, y - 1)] != 1:
                neigh.append((x, y - 1))
            if self.grid[(x, y + 1)] != 1:
                neigh.append((x, y + 1))
            if self.grid[(x - 1, y)] != 1:
                neigh.append((x - 1, y))
            if self.grid[(x + 1, y)] != 1:
                neigh.append((x + 1, y))
        return neigh
    
    def find_path(self, _target, _source, mode = 'A*'):
        """

        :param _target: the location of a target place
        :param _source: the location of a caller
        :param mode:
        :return:
        """
        frontier = [(0, _target)]
        came_from = {}
        cost_so_far = {}
        came_from[_target] = None
        cost_so_far[_target] = 0
        valid = False
        if map.grid[_target] != 0:
            return False
        while len(frontier) > 0:
            frontier.sort(key = itemgetter(0))
            frontier.sort(key = itemgetter(1))
            p, current = frontier.pop(0)
            
            # Early exit
            # if current == _source and mode == 'A*':
            #     return came_from, cost_so_far
            
            # flag valid
            if current == _source:
                valid = True
            
            # todo do I need to sort this too?
            ne = self.neighbours(current)
            ne.sort(key = itemgetter(0))
            ne.sort(key = itemgetter(1))
            for next in ne:
                # todo check for obstacles in next position
                if map.grid[next] != 0:
                    if next != _source:
                        continue
                
                new_cost = cost_so_far[current] + 1  # graph.cost(current, next)
                
                if next not in came_from or new_cost < cost_so_far[next]:
                    cost_so_far[next] = new_cost
                    if mode == 'A*':
                        priority = new_cost + heuristic(_source, next)
                    else:  # elif mode == 'Dijkstra'
                        priority = new_cost
                    # if map.grid[current] != 0:
                    frontier.append((priority, next))
                    came_from[next] = current
        if valid:
            return cost_so_far
            # return came_from, cost_so_far
        else:
            return False
    
    def print_map(self, dic = None):
        map_items = self.grid.items()
        max_y = max([_y for (_x, _y), val in map_items])
        max_x = max([_x for (_x, _y), val in map_items])
        print('\n  ', ''.join([str(k) for k in range(max_x + 1)]))
        for j in range(max_y + 1):
            string = ''
            for i in range(max_x + 1):
                if dic is not None:
                    if (i, j) in dic.keys():
                        string += str(dic[(i, j)])
                    else:
                        string += str(map.grid[(i, j)]).replace('1', '#').replace('0', '.').replace('2', 'G').replace('3', 'E')
                else:
                    string += str(map.grid[(i, j)]).replace('1', '#').replace('0', '.').replace('2', 'G').replace('3', 'E')
            # string = string.replace('1', '#').replace('0', '.').replace('2', 'G').replace('3', 'E')
            print(f"{j:2} {string}")


class Unit:
    def __init__(self, x, y, type, id):
        self.id = id
        self.health = 200
        self.x = x
        self.y = y
        self.type = type
    
    def move(self, destination):
        # destination, path, cost = destination_path
        map.add((self.x, self.y), 0)
        # dest = path.get((self.x, self.y))
        # if dest is not None:
        self.x, self.y = destination
        map.add((self.x, self.y), 3 if self.type == 'E' else 2)


units = []
map = Graph()
with open('d15.txt') as f:
    y = 0
    id = 1
    for line in f:
        x = 0
        for ch in line.strip():
            if ch == "#":
                v = 1
            elif ch == "G":
                v = 2
            elif ch == "E":
                v = 3
            else:
                v = 0
            map.add((x, y), v)
            if ch == "G":
                units.append(Unit(x, y, "G", id))
                id += 1
            elif ch == "E":
                units.append(Unit(x, y, "E", id))
                id += 1
            x += 1
        y += 1

print('read done!')
map.print_map()

round_count = 0


# while len(elves) > 0 and len(goblins) > 0:


def path_test():
    for unit in units:
        in_range = []
        for unit2 in units:
            if unit != unit2:
                for loc in map.neighbours((unit2.x, unit2.y)):
                    in_range.append(loc)
        
        for loc in in_range:
            dist = map.find_path(loc, (unit.x, unit.y))
            
            print(dist)
            # if dist is not False:
            #     map.print_map(dist)


def game():
    premature_combat_end_flag = False
    elves = [unit for unit in units if unit.type == "E"]
    goblins = [unit for unit in units if unit.type == "G"]
    round_count = 0
    while len(elves) > 0 and len(goblins) > 0:
        # for round_count in range(1):
        print(f"==========================\n"
              f"||round {round_count + 1:^5}  started! ||\n"
              f"==========================\n")
        killed_units = []
        units.sort(key = attrgetter("x"))
        units.sort(key = attrgetter("y"))
        unit_locs = [(unit.x, unit.y) for unit in units]
        for unit in units:
            if unit not in killed_units:
                print(f"\n\n==========\tUnit {unit.type}{unit.id:<2} at {unit.x:>2}x{unit.y:<2} turn\t==========")
                # =======movement check be here ===============
                total_enemies = [enemy for enemy in units if unit.type != enemy.type and enemy not in killed_units]
                if len(total_enemies) == 0:
                    print('No enemies left!')
                    premature_combat_end_flag = True
                
                else:
                    # check if there are enemies nearby
                    neighbouring_enemies = [enemy for enemy in units if (enemy.x, enemy.y) in map.neighbours((unit.x, unit.y))
                                            and unit.type != enemy.type and enemy not in killed_units]
                    # if not, try to walk.
                    if len(neighbouring_enemies) == 0:
                        in_range = []
                        reachable = []
                        nearest = []
                        
                        """ Check every enemy, if it has empty surrounding areas,
                            add them to in_range list   """
                        for target in units:
                            if target != unit:
                                if target.type != unit.type:
                                    tx, ty = target.x, target.y
                                    
                                    for loc in map.neighbours((tx, ty)):
                                        # if loc not in unit_locs:
                                        in_range.append(loc)
                        
                        # print(f'{(unit.x, unit.y)} unit {unit.type} has cells {in_range} around all enemies')
                        in_range.sort(key = itemgetter(0))
                        in_range.sort(key = itemgetter(1))
                        """ From those locations, select those are reachable """
                        for place in in_range:
                            
                            # if map.grid[place] == 0:
                            result = map.find_path(place, (unit.x, unit.y))
                            # print(f'{result} is reachable?')
                            
                            """ TODO: FIRST WE NEED TO FIND ALL REACHABLE LOCATIONS
                            
                            THEN SORT THEM BY X, Y
                            
                            THEN FOR A CHOSEN LOCATION SELECT IT'S PATH
                            
                            THEN CHOOSE A CELL FROM UNIT_NEIGHBOUR CELLS WITH LOWEST COST
                            
                            
                            """
                            
                            if result is not False:
                                # map.print_map(result)
                                reachable += [(place, result[coord], coord) for coord in map.neighbours((unit.x, unit.y))
                                              if coord in result.keys()]
                            else:
                                # print("no valid path")
                                pass
                        print(f'Unit {unit.type}{unit.id} at {unit.x:>2}x{unit.y:<2} has {len(reachable)} options to go: {reachable}')
                        
                        """ If there are reachable locations, find those with shortest path to them """
                        if len(reachable) > 0:
                            
                            lowest_cost = min([cost for (place, cost, coord) in reachable])
                            lc_coords = [(place, coord) for (place, cost, coord) in reachable if cost == lowest_cost]
                            lc_coords.sort(key = lambda x: x[1][0])
                            lc_coords.sort(key = lambda x: x[1][1])
                            lc_coords.sort(key = lambda x: x[0][0])
                            lc_coords.sort(key = lambda x: x[0][1])
                            # lc_coords.sort(key = itemgetter(0))
                            # lc_coords.sort(key = itemgetter(1))
                            move_coord = lc_coords[0][1]
                            
                            print(f'Unit {unit.type}{unit.id} at {unit.x:>2}x{unit.y:<2} has {len(lc_coords)} nearest options to go: {lc_coords}'
                                  f' of which {move_coord} is chosen')
                            
                            unit.move(move_coord)
                        else:
                            print("No possible movement!\n")
                    map.print_map()
                    # ========movement logic end now===============
                    
                    # ========COMBAT LOGIC END HERE================
                    
                    neighbouring_enemies = [enemy for enemy in units if (enemy.x, enemy.y) in map.neighbours((unit.x, unit.y))
                                            and unit.type != enemy.type and enemy not in killed_units]
                    combat_string = f"\nRound {round_count + 1} combat phase:\nUnit {unit.type}{unit.id:<2} at {unit.x:>2}x{unit.y:<2} has "
                    if len(neighbouring_enemies) > 0:
                        combat_string += f"{len(neighbouring_enemies)} surrounding enemies:\n"
                        neighbouring_enemies.sort(key = attrgetter("x"))
                        neighbouring_enemies.sort(key = attrgetter("y"))
                        neighbouring_enemies.sort(key = attrgetter("health"))
                        
                        combat_string += f"ids: {[en.id for en in neighbouring_enemies]}"
                        enemy = neighbouring_enemies[0]
                        combat_string += f"\nAttacking enemy {enemy.type}{enemy.id:2} at {enemy.x:>2}x{enemy.y:<2}: " \
                            f"Enemy HP = {enemy.health} -> {enemy.health - 3} "
                        
                        enemy.health -= 3
                        if enemy.health <= 0:
                            combat_string += f"\nEnemy {enemy.id} has been killed!!!"
                            map.add((enemy.x, enemy.y), 0)
                            killed_units.append(enemy)
                    
                    else:
                        combat_string += "no surrounding enemies."
                    
                    print(combat_string)
            
            # ========COMBAT LOGIC END HERE================
        for unit in killed_units:
            if unit in units:
                units.remove(unit)
        
        for unit in units:
            print(f"{unit.type}{unit.id:2}|{unit.x:>2}x{unit.y:<2} : {unit.health:>3}")
        print(f"==========================\n"
              f"||All unites have moved!||\n"
              f"||Round {round_count + 1:^5} has ended!||\n"
              f"==========================\n\n\n")
        map.print_map()
        
        print("\n\n=============================================\n\n")
        elves = [unit for unit in units if unit.type == "E"]
        goblins = [unit for unit in units if unit.type == "G"]
        if not premature_combat_end_flag:
            round_count += 1
    
    print(f"After {round_count} rounds team {units[0].type} has won! \n"
          f"score = {round_count } * {sum([unit.health for unit in units])} ="
          f" {round_count*sum([unit.health for unit in units])} ")


# path_test()
game()
# victory_display()
