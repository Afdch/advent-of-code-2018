from collections import deque


def game3(player_count, marbles_count):
    game_state = deque()
    players = [0 for x in range(player_count)]
    for marble in range(marbles_count + 1):
        if marble > 0 and marble%23 == 0:
            game_state.rotate(-7)
            removed_marble = game_state.popleft()
            players[marble%player_count] += removed_marble + marble
        else:
            game_state.rotate(1)
            game_state.append(marble)
        game_state.rotate(1)
    return max(players)


players_count = 17
marbles_last = 1104
print(game3(players_count, marbles_last))
