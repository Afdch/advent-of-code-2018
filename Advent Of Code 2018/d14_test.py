import unittest
import d14


class TestD9(unittest.TestCase):
    
    def test_run1(self):
        result = d14.part_2(51589)
        self.assertEqual(result, 9)
    
    def test_run2(self):
        result = d14.part_2('01245')
        self.assertEqual(result, 5)
    
    def test_run3(self):
        result = d14.part_2(92510)
        self.assertEqual(result, 18)
    
    def test_run4(self):
        result = d14.part_2(59414)
        self.assertEqual(result, 2018)

    def test_run5(self):
        result = d14.part_2('01301')
        self.assertEqual(result, 5491)
