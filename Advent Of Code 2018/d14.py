def create_receipe(receipe_scores, elf1, elf2, fin = False):
    new_receipes = [int(z) for z in str(receipe_scores[elf1] + receipe_scores[elf2])]
    receipe_scores += new_receipes
    elf1, elf2 = elf1 + receipe_scores[elf1] + 1, elf2 + receipe_scores[elf2] + 1
    while elf1 >= len(receipe_scores):
        elf1 -= len(receipe_scores)
    while elf2 >= len(receipe_scores):
        elf2 -= len(receipe_scores)
    if fin:
        return receipe_scores, elf1, elf2, new_receipes
    else:
        return receipe_scores, elf1, elf2


def part_1(inp):
    receipe_scores = [3, 7]
    elf1 = 0
    elf2 = 1
    last_receips10 = []
    while len(receipe_scores) <= inp - 1:
        receipe_scores, elf1, elf2 = create_receipe(receipe_scores, elf1, elf2)
    else:
        while len(last_receips10) < 10:
            receipe_scores, elf1, elf2, last_receips = create_receipe(receipe_scores, elf1, elf2, True)
            last_receips10 += last_receips
        else:
            print(receipe_scores)
            return last_receips10


def part_2(inp):
    inpt = [int(z) for z in str(inp)]
    receipe_scores = [3, 7]
    elf1 = 0
    elf2 = 1
    while True:
        receipe_scores, elf1, elf2 = create_receipe(receipe_scores, elf1, elf2)
        if receipe_scores[-len(inpt):] == inpt:
            return len(receipe_scores) - len(inpt)
        # Added check below. Problem was the receipes where the sum was 10, both
        # are added but I only check the last added number. So after one receipe
        # creation, something like this 2 [9 3 8 0 1 0] might've occured but I miss
        # the right position. Following check is lazy but leaves most of the code
        # unchanged, so I'll leave it like that.
        if receipe_scores[-len(inpt) - 1:-1] == inpt:
            return len(receipe_scores) - len(inpt) - 1


# print(part_1(40))
print(part_2('293801'))
