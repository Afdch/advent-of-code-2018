from collections import defaultdict
from datetime import datetime

dt = datetime.now()

rules = defaultdict()
pots = set()
initial_state = '#..######..#....#####..###.##..#######.####...####.##..#....#.##.....########.#...#.####........#.#.'

with open("d12.txt") as f:
    for line in f:
        rule = (line.strip().split(' => '))
        rules.update({rule[0]: rule[1]})


for char in range(len(initial_state)):
    if initial_state[char] == '#':
        pots.add(char)

pattern_recognized = False
delta = 0
delta_repeated = 10
sum_pots = sum(pots)
generation = 0
while not pattern_recognized:
    generation += 1
    state = pots.copy()
    for pot in range(min(pots) - 2, max(pots) + 2):
        key = ''
        for shift in range(-2, 3):
            key += '#' if pot + shift in pots else '.'
        if rules.get(key) == '#':
            state.add(pot)
        else:
            if pot in state:
                state.remove(pot)
    pots = state
    # pattern recognition
    this_delta = sum(pots) - sum_pots
    sum_pots = sum(pots)
    if this_delta == delta:
        delta_repeated -= 1
    else:
        delta = this_delta
        delta_repeated = 10
    if delta_repeated == 0:
        pattern_recognized = True
else:
    a = (50000000000 - generation)*delta + sum_pots
    print(f'Answer for part 2 is {a}, took {generation} generations in {datetime.now() - dt}')

print(pots)
count = 0

print(sum(pots))
